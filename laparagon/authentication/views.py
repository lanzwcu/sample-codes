from django.contrib.auth import authenticate, login
from .forms import LoginForm
from django.shortcuts import render, redirect, Http404, get_object_or_404
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User


def login_view(request):
    if request.user.is_authenticated:
        return redirect('home')

    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                if request.user.is_active:
                    if request.GET.get('next'):
                        return redirect(request.GET.get('next'))
                    else:
                        return redirect('home')
                else:
                    return redirect('verify')
            else:
                messages.warning(request, 'Invalid credentials')
    else:
        form = LoginForm()
    return render(request, 'authentication/login.html', {'form': form, 'nbar':'login'})