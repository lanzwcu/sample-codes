from django.shortcuts import render


def home(request):
    return render(request, 'home.html', {'nbar': 'home'})


def about_us(request):
    return render(request, 'about_us.html', {'nbar': 'about_us'})

