from django.shortcuts import render, redirect, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from django.db import IntegrityError
from gallery.models import ArtToy
from .models import Collection, WishList
from .forms import UserRegisterForm
from django.contrib.auth import authenticate, login


def register(request):
    if request.user.is_authenticated:
        return redirect('home')

    if request.method == "POST":
        form = UserRegisterForm(request.POST)
        if form.is_valid():

            user = form.save(commit=False)
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user.is_active = False
            user.set_password(password)
            user.save()

            messages.success(request, 'Account Successfully created!')
            return redirect('authentication:login')
    else:
        form = UserRegisterForm()
    return render(request, "account/register.html", {'form': form})




def collection_page(request):
    collection = Collection.objects.filter(user=request.user)

    paginator = Paginator(collection, 4)
    page = request.GET.get('page')
    try:
        collection = paginator.page(page)
    except PageNotAnInteger:
        collection = paginator.page(1)
    except EmptyPage:
        collection = paginator.page(paginator.num_pages)

    return render(request, 'account/collection.html', {'collection': collection})


def wishlist_page(request):
    wishlist = WishList.objects.filter(user=request.user)

    paginator = Paginator(wishlist, 4)
    page = request.GET.get('page')
    try:
        wishlist = paginator.page(page)
    except PageNotAnInteger:
        wishlist = paginator.page(1)
    except EmptyPage:
        wishlist = paginator.page(paginator.num_pages)

    return render(request, 'account/wishlist.html', {'wishlist': wishlist})





def add_to_profile(request, type_slug, toy_slug):
    art_toy = get_object_or_404(ArtToy, slug=toy_slug)
    user = request.user

    try:
        if type_slug == 'collection':
            Collection.objects.create(
                user=user,
                art_toy=art_toy,
            )
        elif type_slug == 'wishlist':
            WishList.objects.create(
                user=user,
                art_toy=art_toy,
            )
    except IntegrityError:
        messages.warning(request, "Item already in profile.")

    return redirect(request.META['HTTP_REFERER'])


def remove_from_collection(request, toy_slug):
    art_toy = get_object_or_404(ArtToy, slug=toy_slug)
    user = request.user
    Collection.objects.get(
        user=user,
        art_toy=art_toy,
    ).delete()
    return redirect(request.META['HTTP_REFERER'])