from django.db import models
from django.conf import settings
from gallery.models import ArtToy


'''class Profile(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='profile')
    '''


class Collection(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='collection')
    art_toy = models.ForeignKey(ArtToy, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=1)
    acquisition = models.DecimalField(max_digits=9, decimal_places=2, default=0)

    class Meta:
        unique_together = ('user', 'art_toy')


class WishList(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='wish_list')
    art_toy = models.ForeignKey(ArtToy, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('user', 'art_toy')


'''class Trade(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='trade')
    art_toy = models.ForeignKey(ArtToy, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=1)'''

#images