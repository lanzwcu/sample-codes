from django.contrib import admin
from .models import Collection, WishList


class CollectionAdmin(admin.ModelAdmin):
    list_display = ['user', 'art_toy', 'quantity']


class WishListAdmin(admin.ModelAdmin):
    list_display = ['user', 'art_toy']


admin.site.register(Collection, CollectionAdmin)
admin.site.register(WishList, WishListAdmin)

