from django.urls import path

from . import views

app_name = 'account'
urlpatterns = [
    #path('', views.profile_page, name='collection'),
    path('register/', views.register, name='register'),
    #path('profile/', views.profile_page, name='profile'),
    path('profile/collection/', views.collection_page, name='collection'),
    path('profile/wishlist/', views.wishlist_page, name='wishlist'),

    path('add/<slug:type_slug>/<slug:toy_slug>/', views.add_to_profile, name='add_to_profile'),
    path('remove/<slug:toy_slug>/', views.remove_from_collection, name='remove_from_collection'),
]
