from django.db import models
from django_countries.fields import CountryField
from image_cropping import ImageRatioField
from django.utils.text import slugify


def generate_filename(self, filename):
    if self.__class__.__name__ == 'ArtToyImage':
        name = "arttoy/%s/%s" % (self.art_toy.name, filename)
    elif self.__class__.__name__ == 'BaseModelImage':
        name = "basemodel/%s/%s" % (self.base_model.name, filename)
    else:
        name = "%s/%s/%s" % (self.__class__.__name__.lower(), self.name, filename)
    return name


class Organization(models.Model):

    FACEBOOK_GROUP = 'FBG'
    MANUFACTURER = 'MAN'
    RETAILER = 'RET'
    OTHER = 'OTH'
    TYPES = (
        (FACEBOOK_GROUP, 'Facebook Group'),
        (MANUFACTURER, 'Manufacturer'),
        (RETAILER, 'Retailer'),
        (OTHER, 'Other'),
    )

    name = models.CharField(max_length=128, unique=True)
    slug = models.SlugField(max_length=128, unique=True)
    established_on = models.DateField()
    type = models.CharField(max_length=3, choices=TYPES)
    email = models.EmailField(unique=True, blank=True, null=True)
    link = models.URLField(unique=True, blank=True, null=True)
    bio = models.TextField()
    logo = models.ImageField(upload_to=generate_filename)
    # auto
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Event(models.Model):
    name = models.CharField(max_length=128, unique=True)
    organized_by = models.ManyToManyField(Organization, blank=True)
    location = models.CharField(max_length=128, blank=True)
    date = models.DateField()
    link = models.URLField(unique=True, blank=True, null=True)

    def __str__(self):
        return self.name


class Artist(models.Model):
    # user = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=128, unique=True)
    slug = models.SlugField(max_length=128, unique=True)
    alias = models.CharField(max_length=128, blank=True, help_text="Also known as (can be blank); please separate multiple aliases with a comma(,)")
    email = models.EmailField(unique=True, blank=True, null=True)
    logo = models.ImageField(upload_to=generate_filename)
    cropping = ImageRatioField('logo', '450x360')
    country = CountryField(default='PH')
    bio = models.TextField()
    facebook_link = models.URLField(unique=True, blank=True, null=True, default=None)
    instagram_link = models.URLField(unique=True, blank=True, null=True, default=None)
    twitter_link = models.URLField(unique=True, blank=True, null=True, default=None)
    website_link = models.URLField(unique=True, blank=True, null=True,default=None)
    # auto
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        if self.twitter_link == "":
            self.twitter_link = None
        super(Artist, self).save(*args, **kwargs)


#---- Base model of the figure
class BaseModel(models.Model):
    name = models.CharField(max_length=128, unique=True)
    slug = models.SlugField(max_length=128, unique=True)
    designer = models.ManyToManyField(Artist)
    description = models.TextField()
    thumbnail = models.ImageField(upload_to=generate_filename)
    cropping = ImageRatioField('thumbnail', '450x360')
    # auto
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class BaseModelImage(models.Model):
    base_model = models.ForeignKey(BaseModel, on_delete=models.CASCADE)
    image = models.ImageField(upload_to=generate_filename)
    created = models.DateTimeField(auto_now_add=True)


class ArtToy(models.Model):
    OFFICIAL = 'OFF'
    CUSTOM = 'CUS'
    COMMISSION = 'COM'
    TYPES = (
        (OFFICIAL, 'Official'),
        (CUSTOM, 'Custom'),
        (COMMISSION, 'Commission'),
    )
    # art toy details
    type = models.CharField(max_length=3, choices=TYPES, default='OFF')
    name = models.CharField(max_length=128, unique=True)
    slug = models.SlugField(max_length=128, unique=True)
    base_model = models.ManyToManyField(BaseModel, help_text="Please specify the base figure/s used")
    edition_size = models.PositiveIntegerField()
    dimensions = models.CharField(max_length=32, help_text="Please follow this pattern(Lx W x H) and specify the unit of measurement")
    medium = models.CharField(max_length=128, help_text="Materials that were used for the product")
    description = models.TextField(blank=True, help_text="Any relevant details of the art toy.")

    # contributor details
    artist = models.ManyToManyField(Artist)
    organization = models.ManyToManyField(Organization, blank=True, help_text="If there are other organizations involved that is not an artist. eg: Store, FB group, etc.")

    # release details
    event = models.ManyToManyField(Event, blank=True)
    is_auction = models.BooleanField(default=False, help_text="Please specify if this piece was auctioned off.")
    release_date = models.DateField()
    release_at = models.TextField(blank=True, help_text="Please input the links of where the art toy was released.")
    release_price = models.DecimalField(max_digits=9, decimal_places=2, help_text="Please put a value of 0 if the price is unknown.")

    # auto
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    thumbnail = models.ImageField(upload_to=generate_filename)
    cropping = ImageRatioField('thumbnail', '450x360')

    def __str__(self):
        return self.name


class ArtToyImage(models.Model):
    art_toy = models.ForeignKey(ArtToy, default=None, on_delete=models.CASCADE)
    image = models.ImageField(upload_to=generate_filename)
    created = models.DateTimeField(auto_now_add=True)
