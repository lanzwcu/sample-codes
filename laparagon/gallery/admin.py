from django.contrib import admin
from image_cropping import ImageCroppingMixin
from .models import Artist, ArtToy, ArtToyImage, Organization, Event, BaseModel, BaseModelImage

# Art Toy
class ArtToyImageAdmin(admin.StackedInline):
    model = ArtToyImage


@admin.register(ArtToy)
class ArtToyAdmin(ImageCroppingMixin, admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}
    list_display = ['name', 'artists', 'edition_size', 'medium', 'dimensions', 'release_price', 'release_date']
    inlines = [ArtToyImageAdmin]

    def artists(self, obj):
        return "\n".join([a.name for a in obj.artist.all()])

    class Meta:
       model = ArtToy


# Artist
class ArtistAdmin(ImageCroppingMixin, admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}
    list_display = ['name', 'alias', 'email', 'country']


# Base model
class BaseModelImageAdmin(ImageCroppingMixin, admin.StackedInline):
    model = BaseModelImage


@admin.register(BaseModel)
class BaseModelAdmin(ImageCroppingMixin, admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}
    inlines = [BaseModelImageAdmin]

    class Meta:
       model = BaseModel


# Organizations
class OrganizationAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Artist, ArtistAdmin)
admin.site.register(Event)
admin.site.register(Organization, OrganizationAdmin)
admin.site.site_header = "Paragon Admin"
admin.site.site_title = "Paragon Admin Portal"

