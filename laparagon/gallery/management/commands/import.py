from django.core.management.base import BaseCommand, CommandError
from gallery.models import Artist, ArtToy
from datetime import datetime
import csv

class Command(BaseCommand):
    help = "import artist file into model"

    def add_arguments(self, parser):
        parser.add_argument('csv_file', nargs='+', type=str)

    def handle(self, *args, **options):
        for csv_file in options['csv_file']:
            reader = csv.reader(open(csv_file, encoding = 'utf_8'), delimiter=',', quotechar='"')
            next(reader, None)
            for row in reader:
                created = Artist.objects.create(
                    id = row[0],
                    name = row[1],
                    slug = row[2],
                    alias = row[3],
                    email = row[4],
                    logo = row[5],
                    country = row[6],
                    bio = row[7],
                    facebook_link = row[8],
                    instagram_link = row[9],
                    twitter_link = row[10],
                    website_link = row[11],

                )
                if row[10]:
                    print("HAHAHH")
                print("ey" + row[10] + "3")
                if not created:
                    self.stdout.write('Artist "%s" already exists' %row[0])
            self.stdout.write("SUCCESS!")

