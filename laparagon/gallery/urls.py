from django.urls import path

from . import views

app_name = 'gallery'

urlpatterns = [
    path('art_toy/', views.art_toy_page, name='art_toy'),
    path('art_toy/<slug:toy_slug>/', views.art_toy_detail, name='art_toy_detail'),
    path('art_toy_search/', views.art_toy_search, name='art_toy_search'),

    path('artist/', views.artist_page, name='artist'),
    path('artist/<slug:artist_slug>/', views.artist_detail, name='artist_detail'),
    path('artist_search/', views.artist_search, name='artist_search'),

    #path('base_model/', views.base_model_page, name='base_model'),
    path('base_model/<slug:base_slug>/', views.base_model_detail, name='base_model_detail'),

    path('organization/<slug:org_slug>/', views.organization_detail, name='organization_detail'),
    # path('artist/<slug:artist_slug>/', views.artist_detail, name='artist_detail'),
    # path('artist/<slug:artist_slug>/', views.artist_detail, name='artist_detail'),
]