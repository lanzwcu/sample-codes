from django.shortcuts import render, get_object_or_404
from .models import ArtToy, Artist, ArtToyImage, BaseModel, BaseModelImage, Organization
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def base_model_detail(request, base_slug):
    base_model = get_object_or_404(BaseModel, slug=base_slug)
    images = BaseModelImage.objects.filter(base_model=base_model)
    art_toys = ArtToy.objects.filter(base_model=base_model.id)

    paginator = Paginator(art_toys, 4)
    page = request.GET.get('page')
    try:
        art_toys = paginator.page(page)
    except PageNotAnInteger:
        art_toys = paginator.page(1)
    except EmptyPage:
        art_toys = paginator.page(paginator.num_pages)

    return render(request, 'gallery/base_model.html', {'base_model': base_model,
                                                       'art_toys' : art_toys,
                                                       'images' : images})


def art_toy_page(request):
    sort = request.GET.get("sort")
    art_type = request.GET.get("filter")

    type_list = ArtToy.objects.all().distinct('type')

    if sort:
        art_toys = sort_art_toy(sort)
    elif art_type:
        art_toys = filter_art_toy(art_type)
    else:
        art_toys =  ArtToy.objects.all()

    paginator = Paginator(art_toys, 4)
    page = request.GET.get('page')
    try:
        art_toys = paginator.page(page)
    except PageNotAnInteger:
        art_toys = paginator.page(1)
    except EmptyPage:
        art_toys = paginator.page(paginator.num_pages)

    return render(request, 'gallery/art_toy.html', {'art_toys': art_toys,
                                                    'type_list': type_list,
                                                    'nbar': 'art_toy',})


def filter_art_toy(art_type):
    art_toys = ArtToy.objects.filter(type=art_type)

    return art_toys



def sort_art_toy(sort):
    if sort == "title_asc":
        art_toys = ArtToy.objects.all().order_by('name')
    elif sort == "title_des":
        art_toys = ArtToy.objects.all().order_by('-name')
    elif sort == "date_asc":
        art_toys = ArtToy.objects.all().order_by('release_date')
    elif sort == "date_des":
        art_toys = ArtToy.objects.all().order_by('-release_date')
    else:
        art_toys = None

    return art_toys


def artist_page(request):
    sort = request.GET.get("sort")
    nationality = request.GET.get("filter")

    nationality_list = Artist.objects.all().distinct('country').order_by('country')

    if sort:
        artist_list = sort_artist(sort)
    elif nationality:
        artist_list = filter_artist(nationality)
    else:
        artist_list = Artist.objects.all()

    paginator = Paginator(artist_list, 8)
    page = request.GET.get('page')
    try:
        artists = paginator.page(page)
    except PageNotAnInteger:
        artists = paginator.page(1)
    except EmptyPage:
        artists = paginator.page(paginator.num_pages)

    return render(request, 'gallery/artist.html', {'artists': artists,
                                                   'nationality_list': nationality_list,
                                                   'nbar': 'artist',
                                                   'title': 'Artist'})


def filter_artist(nationality):
    artists = Artist.objects.filter(country=nationality)

    return artists



def sort_artist(sort):
    if sort == "name_asc":
        artists = Artist.objects.all().order_by('name')
    elif sort == "name_des":
        artists = Artist.objects.all().order_by('-name')
    else:
        artists = None

    return artists


def art_toy_detail(request, toy_slug):
    toy = get_object_or_404(ArtToy,slug=toy_slug)
    images = ArtToyImage.objects.filter(art_toy=toy.id)

    return render(request, 'gallery/art_toy_detail.html', {'toy': toy,
                                                       'images': images,})


def artist_detail(request, artist_slug):
    artist = get_object_or_404(Artist, slug=artist_slug)
    base_models = BaseModel.objects.filter(designer=artist)

    paginator = Paginator(base_models, 4)
    page = request.GET.get('page')
    try:
        base_models = paginator.page(page)
    except PageNotAnInteger:
        base_models = paginator.page(1)
    except EmptyPage:
        base_models = paginator.page(paginator.num_pages)

    return render(request, 'gallery/artist_detail.html', {'artist': artist,
                                                          'base_models': base_models,})


def art_toy_search(request):
    query = request.GET.get("q")
    if query:
        art_toys_list = ArtToy.objects.filter(Q(name__icontains=query) |
                                              Q(artist__name__icontains=query)).distinct()

        paginator = Paginator(art_toys_list, 4)
        page = request.GET.get('page')
        try:
            art_toys = paginator.page(page)
        except PageNotAnInteger:
            art_toys = paginator.page(1)
        except EmptyPage:
            art_toys = paginator.page(paginator.num_pages)
    else:
        art_toys = None

    sort = request.GET.get("sort")
    if sort:
        art_toys = sort_art_toy(request, sort)

    return render(request, 'gallery/art_toy.html', {'art_toys': art_toys,
                                                    'nbar': 'gallery',})


def artist_search(request):
    query = request.GET.get("q")
    if query:
        artists = Artist.objects.filter(Q(name__icontains=query) |
                                        Q(alias__icontains=query)).distinct()
        paginator = Paginator(artists, 3)
        page = request.GET.get('page')
        try:
            artists = paginator.page(page)
        except PageNotAnInteger:
            artists = paginator.page(1)
        except EmptyPage:
            artists = paginator.page(paginator.num_pages)
    else:
        artists = None
    return render(request, 'gallery/artist.html', {'artists': artists,
                                                   'nbar': 'gallery',})


def organization_detail(request, org_slug):
    organization = get_object_or_404(Organization, slug=org_slug)

    return render(request, 'gallery/organization.html',{'organization': organization})
