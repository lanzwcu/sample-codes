from django.contrib import admin
from bed_management.models import Room, RoomHistoryLog, RoomSession, Step, Zone


class RoomAdmin(admin.ModelAdmin):
    list_display = ('number', 'zone', 'capacity')


class RoomHistoryLogAdmin(admin.ModelAdmin):
    list_display = ('room_session', 'step', 'performed_by', 'timestamp')


class RoomSessionAdmin(admin.ModelAdmin):
    list_display = ('get_session', 'patient', 'active')


class StepAdmin(admin.ModelAdmin):
    list_display = ('name', 'detail', 'zone', 'group')


class ZoneAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')


admin.site.register(Room, RoomAdmin)
admin.site.register(RoomHistoryLog, RoomHistoryLogAdmin)
admin.site.register(RoomSession, RoomSessionAdmin)
admin.site.register(Step, StepAdmin)
admin.site.register(Zone, ZoneAdmin)