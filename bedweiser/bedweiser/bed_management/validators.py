from django.core.exceptions import ValidationError


def validate_range_capacity(value):
    if value not in range(1, 6):
        raise ValidationError('Out of Range')


def validate_range_occupied(value):
    if value not in range(0, 6):
        raise ValidationError('Out of Range')