from bed_management.validators import (
    validate_range_capacity,
    validate_range_occupied,
)
from django.contrib.auth.models import Group
from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class Zone(models.Model):
    name = models.CharField(max_length=50, unique=True)
    description = models.TextField()

    def __str__(self):
        return self.name


class Step(models.Model):
    name = models.CharField(max_length=50, unique=True)
    detail = models.TextField()
    zone = models.ForeignKey(
        Zone, related_name='step', on_delete=models.CASCADE
    )
    group = models.ForeignKey(Group)

    def __str__(self):
        return self.name


class Room(models.Model):
    number = models.CharField(max_length=10, unique=True)
    zone = models.ForeignKey(
        Zone, related_name='room', on_delete=models.CASCADE
    )
    capacity = models.IntegerField(
        default=1, validators=[validate_range_capacity]
    )

    def __str__(self):
        return self.number


class RoomSession(models.Model):
    room = models.ForeignKey(
        Room, related_name='room', on_delete=models.CASCADE
    )
    bed_number = models.CharField(max_length=4)
    patient = models.ForeignKey(
        'patientrecords.Patient', null=True, related_name='patient',
        blank=True, on_delete=models.CASCADE
    )
    active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    date_occupied = models.DateTimeField(
        null=True, blank=True, editable=False
    )
    date_discharged = models.DateTimeField(
        null=True, blank=True, editable=False
    )

    class Meta:
        verbose_name_plural = 'Room Sessions'

    def get_session(self):
        return self.room.number + '-' + self.bed_number
    get_session.short_description = "Room Session"

    def __str__(self):
        return self.get_session()


class RoomHistoryLog(models.Model):
    room_session = models.ForeignKey(
        RoomSession, related_name='room_session', on_delete=models.CASCADE
    )
    step = models.ForeignKey(
        Step, related_name='step', on_delete=models.CASCADE
    )
    performed_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, blank=True,
        related_name='performed_by', on_delete=models.CASCADE
    )
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'Room History Logs'

    def __str__(self):
        return self.room_session.room.number
