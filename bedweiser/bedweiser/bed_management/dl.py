from bed_management.models import Room, RoomHistoryLog, RoomSession, Step, Zone
from django.shortcuts import get_object_or_404
from django.utils import timezone
from patientrecords.models import Patient


def list_zones():
    zones = Zone.objects.all()
    return zones


def list_rooms_in_zone(zone_id):
    zone = get_object_or_404(Zone, pk=zone_id)
    rooms = Room.objects.filter(zone=zone)
    return zone, rooms


def list_steps_in_zone(zone_id):
    zone = get_object_or_404(Zone, pk=zone_id)
    steps = Step.objects.filter(zone=zone)
    return zone, steps


def list_room_sessions():
    sessions = RoomSession.objects.filter(active=True)
    return sessions


def create_session(room_id, bed_number):
    room = get_object_or_404(Room, pk=room_id)

    open_sessions_in_room = RoomSession.objects.filter(room=room,
                                                       active=True).count()
    if room.capacity > open_sessions_in_room:
        room_session, created = RoomSession.objects.get_or_create(
            room=room,
            bed_number=bed_number,
            active=True,
        )
        if created:
            return created
        return ''
    return False


def associate_session(pk, patient_his_id):
    patient = get_object_or_404(Patient, his_id=patient_his_id)
    session = get_object_or_404(RoomSession, pk=pk)

    if session.patient:
        return False

    try:
        session = RoomSession.objects.get(patient=patient, active=True)
        return ''
    except RoomSession.DoesNotExist:
        session.date_occupied = timezone.now()
        session.patient = patient
        session.save()
        return patient


def deactivate_session(pk):
    session = get_object_or_404(RoomSession, pk=pk, active=True)
    if session.patient:
        session.date_discharged = timezone.now()
    session.active = False
    session.save()
    return session


def view_steps_performed(pk):
    session = get_object_or_404(RoomSession, pk=pk)
    steps_performed = RoomHistoryLog.objects.filter(room_session=session)
    return steps_performed


def perform_step(pk, step_id, user):
    step = get_object_or_404(Step, pk=step_id)
    room_session = get_object_or_404(RoomSession, pk=pk, active=True)

    if user.is_superuser or user.groups.filter(name=step.group.name).exists():
        performed = RoomHistoryLog.objects.create(
            room_session=room_session,
            step=step,
            performed_by=user,
        )
        return performed
    return ''
