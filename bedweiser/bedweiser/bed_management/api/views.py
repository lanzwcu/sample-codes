from bed_management.api.pagination import DefaultPageNumberPagination
from bed_management.api.serializers import (
    ZoneListSerializer,
    RoomsInZoneSerializer,
    StepsInZoneSerializer,
    RoomSessionListSerializer,
    CreateSerializer,
    AssociatePatientSerializer,
    DeactivateRoomSessionSerializer,
    PerformStepSerializer,
    StepsPerformedSerializer,
)
from bed_management.dl import (
    list_zones,
    list_rooms_in_zone,
    list_steps_in_zone,
    create_session,
    associate_session,
    deactivate_session,
    perform_step,
    list_room_sessions,
    view_steps_performed,
)
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_400_BAD_REQUEST,
)


class ZoneList(ListAPIView):
    permission_classes = [AllowAny]
    pagination_class = DefaultPageNumberPagination
    serializer_class = ZoneListSerializer
    queryset = list_zones()


class RoomsInZone(ListAPIView):
    permission_classes = [AllowAny]
    pagination_class = DefaultPageNumberPagination
    serializer_class = RoomsInZoneSerializer

    def get_queryset(self):
        zone_id = self.kwargs['zone_id']
        zone, rooms = list_rooms_in_zone(zone_id)
        return rooms


class StepsInZone(ListAPIView):
    permission_classes = [AllowAny]
    pagination_class = DefaultPageNumberPagination
    serializer_class = StepsInZoneSerializer

    def get_queryset(self):
        zone_id = self.kwargs['zone_id']
        zone, steps = list_steps_in_zone(zone_id)
        return steps


class RoomSession(ListAPIView):
    pagination_class = DefaultPageNumberPagination
    serializer_class = RoomSessionListSerializer
    queryset = list_room_sessions()

    def post(self, request):
        serializer = CreateSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            room_id = serializer.validated_data['room_id']
            bed_number = serializer.validated_data['bed_number']

            created = create_session(room_id, bed_number)

            if created:
                return Response(status=HTTP_201_CREATED)
            elif created == '':
                message = 'Session already exists.'
                return Response({'message': message},
                                status=HTTP_400_BAD_REQUEST)
            else:
                message = 'Room is already full.'
                return Response({'message': message},
                                status=HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class AssociatePatient(RetrieveAPIView):
    pagination_class = DefaultPageNumberPagination
    serializer_class = AssociatePatientSerializer
    queryset = list_room_sessions()

    def patch(self, request, pk):
        serializer = AssociatePatientSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            patient = serializer.validated_data['patient']

            patient = associate_session(pk, patient['his_id'])

            if patient:
                return Response(status=HTTP_200_OK)
            elif patient == '':
                message = 'Patient already has a session.'
                return Response({'message': message},
                                status=HTTP_400_BAD_REQUEST)
            else:
                message = 'Session already has a patient'
                return Response({'message': message},
                                status=HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class DeactivateSession(RetrieveAPIView):
    serializer_class = DeactivateRoomSessionSerializer
    queryset = list_room_sessions()

    def patch(self, request, pk):
        deactivate_session(pk)
        return Response(status=HTTP_200_OK)


class StepsPerformed(ListAPIView):
    pagination_class = DefaultPageNumberPagination
    serializer_class = StepsPerformedSerializer

    def get_queryset(self):
        pk = self.kwargs['pk']
        steps_performed = view_steps_performed(pk)
        return steps_performed

    def post(self, request, pk):
        serializer = PerformStepSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = request.user
            step = serializer.validated_data['step']

            performed = perform_step(pk, step.id, user)

            if performed:
                return Response(status=HTTP_201_CREATED)
            else:
                message = 'User is not authorized to perform step.'
                return Response({'message': message},
                                status=HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)