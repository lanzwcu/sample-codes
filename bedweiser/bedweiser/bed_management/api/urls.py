from bed_management.api.views import (
    RoomsInZone,
    StepsInZone,
    ZoneList,
    RoomSession,
    AssociatePatient,
    DeactivateSession,
    StepsPerformed,
)
from django.conf.urls import url


urlpatterns = [
    url(r'^zones/$',
        ZoneList.as_view(), name='zones'),
    url(r'^zones/(?P<zone_id>\d+)/rooms/$',
        RoomsInZone.as_view(), name='rooms-in-zone'),
    url(r'^zones/(?P<zone_id>\d+)/steps/$',
        StepsInZone.as_view(), name='steps-in-zone'),
    url(r'^roomsessions/$',
        RoomSession.as_view(), name='room-sessions'),
    url(r'^roomsessions/(?P<pk>\d+)/associate/$',
        AssociatePatient.as_view(), name='associate-patient'),
    url(r'^roomsessions/(?P<pk>\d+)/deactivate/$',
        DeactivateSession.as_view(), name='deactivate-session'),
    url(r'^roomsessions/(?P<pk>\d+)/steps/$',
        StepsPerformed.as_view(), name='view-steps-performed'),
]
