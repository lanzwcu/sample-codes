from bed_management.models import Room, RoomHistoryLog, RoomSession, Step, Zone
from rest_framework.serializers import (
    CharField,
    Serializer,
    ModelSerializer,
    HyperlinkedIdentityField,
    SerializerMethodField,
)
from rest_framework.reverse import reverse


class ParameterisedHyperlinkedIdentityField(HyperlinkedIdentityField):
    lookup_fields = (('pk', 'pk'),)

    def __init__(self, *args, **kwargs):
        self.lookup_fields = kwargs.pop('lookup_fields', self.lookup_fields)
        super(ParameterisedHyperlinkedIdentityField,
              self).__init__(*args, **kwargs)

    def get_url(self, obj, view_name, request, format):
        kwargs = {}
        for model_field, url_param in self.lookup_fields:
            attr = obj
            for field in model_field.split('.'):
                attr = getattr(attr, field)
            kwargs[url_param] = attr

        return reverse(view_name, kwargs=kwargs,
                       request=request, format=format)


class ZoneListSerializer(ModelSerializer):
    view_rooms = HyperlinkedIdentityField(
        view_name='bed_management-api:rooms-in-zone',
        lookup_url_kwarg='zone_id'
    )
    view_steps = HyperlinkedIdentityField(
        view_name='bed_management-api:steps-in-zone',
        lookup_url_kwarg='zone_id'
    )

    class Meta:
        model = Zone
        fields = ['name', 'description', 'view_rooms', 'view_steps']


class RoomsInZoneSerializer(ModelSerializer):
    class Meta:
        model = Room
        fields = ['number']


class StepsInZoneSerializer(ModelSerializer):
    class Meta:
        model = Step
        fields = ['name']


class RoomSessionListSerializer(ModelSerializer):
    associate_patient = HyperlinkedIdentityField(
        view_name='bed_management-api:associate-patient',
        lookup_url_kwarg='pk'
    )
    view_steps_performed = HyperlinkedIdentityField(
        view_name='bed_management-api:view-steps-performed',
        lookup_url_kwarg='pk'
    )
    deactivate_session = HyperlinkedIdentityField(
        view_name='bed_management-api:deactivate-session',
        lookup_url_kwarg='pk'
    )

    class Meta:
        model = RoomSession
        fields = ['get_session', 'associate_patient', 'view_steps_performed',
                  'deactivate_session']


class AssociatePatientSerializer(ModelSerializer):
    patient_his_id = CharField(source='patient.his_id', allow_null=True)

    class Meta:
        model = RoomSession
        fields = ['get_session', 'patient_his_id']


class DeactivateRoomSessionSerializer(ModelSerializer):
    class Meta:
        model = RoomSession
        fields = ['get_session', 'active']


class StepsPerformedSerializer(ModelSerializer):
    room_session = SerializerMethodField()
    step = SerializerMethodField()
    performed_by = SerializerMethodField()

    class Meta:
        model = RoomHistoryLog
        fields = ['room_session', 'step', 'performed_by']

    def get_room_session(self, obj):
        return str(obj.room_session)

    def get_step(self, obj):
        return str(obj.step)

    def get_performed_by(self, obj):
        return str(obj.performed_by)


class PerformStepSerializer(ModelSerializer):
    class Meta:
        model = RoomHistoryLog
        fields = ['step']


class CreateSerializer(Serializer):
    room_id = CharField(max_length=10)
    bed_number = CharField(max_length=4)

