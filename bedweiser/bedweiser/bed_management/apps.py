from django.apps import AppConfig


class BedManagementConfig(AppConfig):
    name = 'bed_management'
