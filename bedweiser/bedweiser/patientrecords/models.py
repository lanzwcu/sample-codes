import datetime

from django.db import models


class Patient(models.Model):

    MALE = 'M'
    FEMALE = 'F'
    GENDERS = (
        (MALE, 'Male'),
        (FEMALE, 'Female')
    )

    given_name = models.CharField(
        max_length=64,blank=True, verbose_name='Given Name'
    )
    middle_name = models.CharField(
        max_length=64, blank=True, verbose_name='Middle Name'
    )
    last_name = models.CharField(
        max_length=64, blank=True, verbose_name='Last Name'
    )
    birth_date = models.DateField(
        blank=True, null=True, verbose_name='Birth Date'
    )
    gender = models.CharField(
        max_length=4, blank=True, choices=GENDERS
    )
    his_id = models.CharField(
        max_length=128, blank=True, null=True, unique=True,
        verbose_name='HIS Identifier'
    )

    class Meta:
        verbose_name = 'Patient'
        verbose_name_plural = 'Patients'

    def __str__(self):
        if self.last_name and self.given_name:
            return self.last_name + ', ' + self.given_name
        else:
            return 'Anonymous Patient'

    def get_age(self):
        if self.birth_date:
            birthday = self.birth_date
            today = datetime.date.today()
            return today.year - birthday.year - int(
                (today.month, today.day) < (birthday.month, birthday.day)
            )
        return ''
    get_age.short_description = "Age"

    def save(self, *args, **kwargs):
        self.given_name = self.given_name.title()
        self.middle_name = self.middle_name.title()
        self.last_name = self.last_name.title()
        super(Patient, self).save(*args, **kwargs)
