from django.contrib import admin

from patientrecords.models import Patient


class PatientAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'birth_date', 'get_age', 'gender')
    search_fields = ['last_name', 'given_name', 'his_id']

admin.site.register(Patient, PatientAdmin)
