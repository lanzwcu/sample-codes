export DEBIAN_FRONTEND=noninteractive

apt-get update

apt-get upgrade -y

apt-get install -y git

apt-get install -y python3 python3-pip

apt-get install -y postgresql

dpkg --configure -a

apt-get install -y python3-sphinx

cd projects

sudo pip3 install -r requirements.txt

# Create DB
sudo -u postgres psql -c "CREATE DATABASE bedweiser_db;"

# Create user
sudo -u postgres psql -c "CREATE USER novostorm WITH PASSWORD 'password';"

# Grant privilege
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE bedweiser_db TO novostorm;"

cd bedweiser

# Database migration
python3 manage.py migrate