export DEBIAN_FRONTEND=noninteractive

apt-get update

apt-get upgrade -y

apt-get install -y git

apt-get install -y python3 python3-pip

apt-get install -y python3-sphinx
