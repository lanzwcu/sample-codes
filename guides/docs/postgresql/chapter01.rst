1 - Database
============
A database is basically a collection of data stored in a server. You can store,
manipulate and retrieve data from it. It contains data in a form of a table
like this:

+----+------+----------------+
| id | name | email          |
+====+======+================+
| 1  | John | john@gmail.com |
+----+------+----------------+
| 2  | Doe  | doe@yahoo.com  |
+----+------+----------------+

We have to use SQL queries in order to retrieve or manipulate the data. When
using SQL commands make sure to end with a semicolon ``;``, this tells SQL
where the command stops. It's like a period to end a sentence.


1.1 Create a Database
---------------------
.. _above:

**CREATE DATABASE**
  This command is used to create a new database.

    syntax: ``CREATE DATABASE database_name``

    example: ``CREATE DATABASE test_database;``

  .. code-block:: psql

    postgres=# CREATE DATABASE test_database;
    CREATE DATABASE


_______________________________________________________________________________

``postgres`` account is a superuser, to create a database with an owner specified
we first have to create a role then declare the owner upon creating the database:

**CREATE ROLE**
  This command is used to create a new role.

    syntax: ``CREATE ROLE role_name WITH option/s``

    example: ``CREATE ROLE test_role WITH LOGIN ENCRYPTED PASSWORD 'password';``

  .. code-block:: psql

    postgres=# CREATE ROLE test_role WITH LOGIN ENCRYPTED PASSWORD 'password';
    CREATE ROLE


  Now let's specify the owner upon creating the database:

    syntax: ``CREATE DATABASE db_name OWNER = owner_name``

    example: ``CREATE DATABASE db_name OWNER = test_role;``

  .. code-block:: psql

    postgres=# CREATE DATABASE db_name OWNER =  test_role;
    CREATE DATABASE


.. note:: You must logged in as a superuser or have ``CREATEDB`` role to be able to
          create a database. Otherwise you will receive this error:
          ``ERROR:  permission denied to create database``


1.2 Alter a Database
--------------------
**ALTER DATABASE**
  This command is used to change the name, owner, tablespace of a database,

    syntax: ``ALTER DATABASE database_name``

    example: ``ALTER DATABASE test_database RENAME TO changed_name;``

  .. code-block:: psql

    postgres=# ALTER DATABASE test_database RENAME TO changed_name;
    ALTER DATABASE


1.3 Drop a Database
-------------------
**DROP DATABASE**
  This command is used to delete or drop an existing database.

    syntax: ``DROP DATABASE database_name``

    example: ``DROP DATABASE test_database;``

  .. code-block:: psql

    postgres=# DROP DATABASE test_database;
    DROP DATABASE

.. note:: You must be logged in as the owner of the database or as a superuser
          to be able to drop a database. Otherwise you will receive this error:
          ``ERROR:  must be owner of database``

.. warning:: Be mindful when dropping a database as it will wipe out all the data
             along with it.
