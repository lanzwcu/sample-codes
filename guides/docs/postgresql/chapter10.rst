10 - Information Schema
========================
Information schema is a schema named information_schema, with the underscore.
This schema exists in all databases automatically. By default, the owner of the
schema is the initial database user in the cluster. That user has all privileges
on this schema.


10.1 Information Schema Data Types
----------------------------------
The columns of the information schema views uses special data types that are
defined in the information schema. Every column in the information schema has
one of these types:

- ``cardinal_numer`` - a non negative integer
- ``character_Data`` - a character string without a specific max length
- ``sql_identifier`` - a character string specifically used for SQL identifiers
- ``time_Stamp`` - a timestamp with time zone
- ``yes_or_no`` - a character string that contains either 'YES' or 'NO', similar to ``Boolean`` that contains either 'true' or 'false'


10.2 Tables in Information Schema
---------------------------------
Below are some of the frequently used tables in ``information_schema``:

**information_schema_catalog_name**
  This is a table that always contains one row and one column containing the
  name of the current database.

  .. code-block:: psql

    postgres=# SELECT * FROM information_schema.information_schema_catalog_name;
     catalog_name
    --------------
     postgres
    (1 row)


_______________________________________________________________________________

**administrable_role_authorizations**
  This table identifies all roles that the current user has the admin option for.

  - grantee - name of the role which this role membership was granted
  - role_name - name of a role
  - is_grantable - always YES

  .. code-block:: psql

    postgres=# SELECT * FROM information_schema.administrable_role_authorizations;
     grantee | role_name | is_grantable
    ---------+-----------+--------------
    (0 rows)


_______________________________________________________________________________

**applicable_roles**
  This table identifies all roles whose privileges the current user can use. The
  current useris also an applicable role but it is not listed.

  .. code-block:: psql

    postgres=# SELECT * FROM information_schema.applicable_roles;
      grantee   |      role_name       | is_grantable
    ------------+----------------------+--------------
     pg_monitor | pg_read_all_settings | NO
     pg_monitor | pg_read_all_stats    | NO
     pg_monitor | pg_stat_scan_tables  | NO
    (3 rows)


_______________________________________________________________________________

**columns**
  This table contains information about the columns in each table in the database.
  It only shows the columns that the current user has access to. Below are some
  of the columns it contain:

  - table_catalog - name of the database containing the table
  - table_name - name of the table
  - column_name - name of the column
  - data_type - the data type of the column
  - udt_name - name of the column data type
  - is_updatable - YES if the column is updatable, NO if otherwise

  .. code-block:: psql

    postgres=# postgres=# SELECT * FROM information_schema.columns;
     table_catalog |    table_schema    |              table_name               |             column_name             | ordinal_position |           column_default           | is_nullable |        data_type         | character_maximum_length | character_octet_length | numeric_precision | numeric_precision_radix | numeric_scale | datetime_precision | interval_type | interval_precision | character_set_catalog | character_set_schema | character_set_name | collation_catalog | collation_schema | collation_name | domain_catalog |   domain_schema    |   domain_name   | udt_catalog | udt_schema |    udt_name     | scope_catalog | scope_schema | scope_name | maximum_cardinality | dtd_identifier | is_self_referencing | is_identity | identity_generation | identity_start | identity_increment | identity_maximum | identity_minimum | identity_cycle | is_generated | generation_expression | is_updatable
    ---------------+--------------------+---------------------------------------+-------------------------------------+------------------+------------------------------------+-------------+--------------------------+--------------------------+------------------------+-------------------+-------------------------+---------------+--------------------+---------------+--------------------+-----------------------+----------------------+--------------------+-------------------+------------------+----------------+----------------+--------------------+-----------------+-------------+------------+-----------------+---------------+--------------+------------+---------------------+----------------+---------------------+-------------+---------------------+----------------+--------------------+------------------+------------------+----------------+--------------+-----------------------+--------------
     postgres      | pg_catalog         | pg_proc                               | proname                             |                1 |                                    | NO          | name                     |                          |                        |                   |                         |               |                    |               |                    |                       |                      |                    |                   |                  |                |                |                    |                 | postgres    | pg_catalog | name            |               |              |            |                     | 1              | NO                  | NO          |                     |                |                    |                  |                  | NO             | NEVER        |                       | YES
     postgres      | pg_catalog         | pg_proc                               | pronamespace                        |                2 |                                    | NO          | oid                      |                          |                        |                   |                         |               |                    |               |                    |                       |                      |                    |                   |                  |                |                |                    |                 | postgres    | pg_catalog | oid             |               |              |            |                     | 2              | NO                  | NO          |                     |                |                    |                  |                  | NO             | NEVER        |                       | YES
     postgres      | pg_catalog         | pg_proc                               | proowner                            |                3 |                                    | NO          | oid                      |                          |                        |                   |                         |               |                    |               |                    |                       |                      |                    |                   |                  |                |                |                    |                 | postgres    | pg_catalog | oid             |               |              |            |                     | 3              | NO                  | NO          |                     |                |                    |                  |                  | NO             | NEVER        |                       | YES

_______________________________________________________________________________

**column_privileges**
  This table shows all privileges granted on columns to a role.

  - grantor - name of the role who granted the privilege
  - grantee - name of the role that the privilege was granted to
  - privilege_type - the type of privilege like: ``SELECT``, ``INSERT``, ``UPDATE``, etc.

  .. code-block:: psql

    postgres=# SELECT * FROM information_schema.column_privileges;
     grantor  | grantee  | table_catalog |    table_schema    |              table_name               |             column_name             | privilege_type | is_grantable
    ----------+----------+---------------+--------------------+---------------------------------------+-------------------------------------+----------------+--------------
     postgres | postgres | postgres      | pg_catalog         | pg_stat_progress_vacuum               | datid                               | SELECT         | YES
     postgres | postgres | postgres      | pg_catalog         | pg_namespace                          | nspname                             | SELECT         | YES
     postgres | postgres | postgres      | pg_catalog         | pg_type                               | typelem                             | INSERT         | YES
     postgres | PUBLIC   | postgres      | pg_catalog         | pg_type                               | typcategory                         | SELECT         | NO
     postgres | postgres | postgres      | information_schema | columns                               | table_name                          | REFERENCES     | YES


_______________________________________________________________________________

**constraint_column_usage**
  This table shows all columns in the current database that are used by the same
  constraint.

  - constraint_catalog - name of the database that contains the constraint (always the current database)
  - constraint_schema = name of the schema that contains the constraint
  - constraint_name - name of the constraint like: ``PRIMARY KEY``, ``FOREIGN KEY``, etc.


  .. code-block:: psql

    postgres=# SELECT * FROM information_schema.constraint_column_usage;
     table_catalog | table_schema | table_name | column_name | constraint_catalog | constraint_schema |   constraint_name
    ---------------+--------------+------------+-------------+--------------------+-------------------+---------------------
     postgres      | public       | author     | id          | postgres           | public            | author_pkey
     postgres      | public       | book       | id          | postgres           | public            | book_pkey
     postgres      | public       | author     | id          | postgres           | public            | book_author_id_fkey
    (3 rows)

_______________________________________________________________________________

**key_column_usage**
  This table shows all columns in the current database that are restricted by some
  unique, primary key or foreign key constraint.

  - ordinal_position - ordinal position of the column within the constraint key (starts at 1)
  - position_in_unique_constraint - foreign key starts at 1, otherwise ``NULL``

  .. code-block:: psql

    postgres=# SELECT * FROM information_schema.key_column_usage;
     constraint_catalog | constraint_schema |   constraint_name   | table_catalog | table_schema | table_name | column_name | ordinal_position | position_in_unique_constraint
    --------------------+-------------------+---------------------+---------------+--------------+------------+-------------+------------------+-------------------------------
     postgres           | public            | author_pkey         | postgres      | public       | author     | id          |                1 |
     postgres           | public            | book_pkey           | postgres      | public       | book       | id          |                1 |
     postgres           | public            | book_author_id_fkey | postgres      | public       | book       | author_id   |                1 |                             1
    (3 rows)


_______________________________________________________________________________

**schemata**
  This table contains all schemas in the current database that the current user
  has access to. The last 4 columns are ``NULL`` as it applies to a feature not
  available in PostgreSQL.

  .. code-block:: psql

    postgres=# SELECT * FROM information_schema.schemata;
     catalog_name |    schema_name     | schema_owner | default_character_set_catalog | default_character_set_schema | default_character_set_name | sql_path
    --------------+--------------------+--------------+-------------------------------+------------------------------+----------------------------+----------
     postgres     | pg_toast           | postgres     |                               |                              |                            |
     postgres     | pg_temp_1          | postgres     |                               |                              |                            |
     postgres     | pg_toast_temp_1    | postgres     |                               |                              |                            |
     postgres     | pg_catalog         | postgres     |                               |                              |                            |
     postgres     | public             | postgres     |                               |                              |                            |
     postgres     | information_schema | postgres     |                               |                              |                            |
    (6 rows)


_______________________________________________________________________________

**sequences**
  This table contains all sequences in the current database that the current user
  has access to.

  .. code-block:: psql

    postgres=# SELECT * FROM information_schema.sequences;
     sequence_catalog | sequence_schema | sequence_name | data_type | numeric_precision | numeric_precision_radix | numeric_scale | start_value | minimum_value |    maximum_value    | increment | cycle_option
    ------------------+-----------------+---------------+-----------+-------------------+-------------------------+---------------+-------------+---------------+---------------------+-----------+--------------
     postgres         | public          | author_id_seq | bigint    |                64 |                       2 |             0 | 1           | 1             | 9223372036854775807 | 1         | NO
     postgres         | public          | book_id_seq   | bigint    |                64 |                       2 |             0 | 1           | 1             | 9223372036854775807 | 1         | NO
    (2 rows)


.. note:: The start, minimum, maximum, and increment values are returned as
          character strings in accordance with the SQL standard.


_______________________________________________________________________________

**table_constraints**
  This table shows all the constraints in all tables that the current user own
  or has ``SELECT`` privilege.

  .. code-block:: psql

    postgres=# SELECT * FROM information_schema.table_constraints;
     constraint_catalog | constraint_schema |    constraint_name    | table_catalog | table_schema |       table_name        | constraint_type | is_deferrable | initially_deferred
    --------------------+-------------------+-----------------------+---------------+--------------+-------------------------+-----------------+---------------+--------------------
     postgres           | public            | author_pkey           | postgres      | public       | author                  | PRIMARY KEY     | NO            | NO
     postgres           | public            | book_pkey             | postgres      | public       | book                    | PRIMARY KEY     | NO            | NO
     postgres           | public            | book_author_id_fkey   | postgres      | public       | book                    | FOREIGN KEY     | NO            | NO
     postgres           | pg_catalog        | 11_1255_1_not_null    | postgres      | pg_catalog   | pg_proc                 | CHECK           | NO            | NO


_______________________________________________________________________________

**table_privileges**
  This table contains all privileges granted on tables to a currently enable role.
  There is one row for each combination of table, grantor and grantee.


  .. code-block:: psql

    postgres=# SELECT * FROM information_schema.table_privileges;
    grantor  | grantee  | table_catalog |    table_schema    |              table_name               | privilege_type | is_grantable | with_hierarchy
    ----------+----------+---------------+--------------------+---------------------------------------+----------------+--------------+----------------
     vagrant  | vagrant  | postgres      | public             | author                                | INSERT         | YES          | NO
     vagrant  | vagrant  | postgres      | public             | author                                | SELECT         | YES          | YES
     vagrant  | vagrant  | postgres      | public             | author                                | UPDATE         | YES          | NO
     vagrant  | vagrant  | postgres      | public             | author                                | DELETE         | YES          | NO


_______________________________________________________________________________

**tables**
  This table contains all tables in the current database that the current user
  has access to.

  .. code-block:: psql

    postgres=# postgres=# SELECT * FROM information_schema.tables;
     table_catalog |    table_schema    |              table_name               | table_type | self_referencing_column_name | reference_generation | user_defined_type_catalog | user_defined_type_schema | user_defined_type_name | is_insertable_into | is_typed | commit_action
    ---------------+--------------------+---------------------------------------+------------+------------------------------+----------------------+---------------------------+--------------------------+------------------------+--------------------+----------+---------------
     postgres      | pg_catalog         | pg_statistic                          | BASE TABLE |                              |                      |                           |                          |                        | YES                | NO       |
     postgres      | pg_catalog         | pg_type                               | BASE TABLE |                              |                      |                           |                          |                        | YES                | NO       |
     postgres      | public             | author                                | BASE TABLE |                              |                      |                           |                          |                        | YES                | NO       |


_______________________________________________________________________________

**views**
  This table contains all views in the current database that the current user
  has access to.

  .. code-block:: psql

    postgres=# SELECT * FROM information_schema.views;
     table_catalog |    table_schema    |              table_name               |                                                                                                                                                                                                                                                                                                              view_definition                                                                                                                                                                                                                                                                                                               | check_option | is_updatable | is_insertable_into | is_trigger_updatable | is_trigger_deletable | is_trigger_insertable_into
    ---------------+--------------------+---------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------+--------------+--------------------+----------------------+----------------------+----------------------------
     postgres      | pg_catalog         | pg_shadow                             |  SELECT pg_authid.rolname AS usename,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     +| NONE         | NO           | NO                 | NO                   | NO                   | NO
                   |                    |                                       |     pg_authid.oid AS usesysid,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            +|              |              |                    |                      |                      |
                   |                    |                                       |     pg_authid.rolcreatedb AS usecreatedb,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 +|              |              |                    |                      |                      |

