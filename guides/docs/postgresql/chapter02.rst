2 - Table
=========
Multiple tables can reside inside one database, it consist of columns and rows.
Columns contain the column name and data type, while the rows contains the records
for the columns.


2.1 Create a Table
------------------
**CREATE TABLE**
  This command is used to create a table, you have to specify the column name
  and its data type.

    syntax: ``CREATE TABLE table_name(column_name data_type constraints_if_any)``

    example: ``CREATE TABLE author (id int, first_name varchar(50), last_name
    varchar(50), birthday date, gender varchar(6));``

  .. code-block:: psql

    postgres=# CREATE TABLE author (
    id int,
    first_name varchar(50),
    last_name varchar(50),
    birthday date,
    gender varchar(6)
    );
    CREATE TABLE


.. note:: To browse through the different data types of postgres, please visit:
          https://www.postgresql.org/docs/10/datatype.html


2.2 Alter a Table
-----------------
**ALTER TABLE**
  This command is used to alter the definition of a table. It is used to
  add, delete or modify the columns in a table.

  When adding a column:

    syntax 1: ``ALTER TABLE table_name ADD column_name data_type constraints_if_any``

    example 1: ``ALTER TABLE author ADD email varchar(50);``

  .. code-block:: psql

    postgres=# ALTER TABLE author ADD email varchar(50);
    ALTER TABLE


_______________________________________________________________________________

  When changing the data type of a column:

    syntax 2: ``ALTER TABLE table_name ALTER COLUMN column_name TYPE data_type``

    example 2: ``ALTER TABLE author ALTER COLUMN email TYPE varchar(150);``

  .. code-block:: psql

    postgres=# ALTER TABLE author ALTER COLUMN email TYPE varchar(150);
    ALTER TABLE

_______________________________________________________________________________

  When deleting a column:

    syntax 3: ``ALTER TABLE table_name DROP COLUMN column_name``

    example 3: ``ALTER TABLE author DROP COLUMN email;``

  .. code-block:: psql

    postgres=# ALTER TABLE author DROP COLUMN email;
    ALTER TABLE


2.3 Drop and Truncate a Table
-----------------------------
**DROP TABLE**
  This command is used to delete an existing table.

    syntax: ``DROP TABLE table_name``

    example ``DROP TABLE author;``

  .. code-block:: psql

    postgres=# DROP TABLE author;
    DROP TABLE


_______________________________________________________________________________

**TRUNCATE TABLE**
  This command is used to wipe out the contents of a table, it does not delete
  the table itself.

      syntax: ``TRUNCATE TABLE table_name``

      example ``TRUNCATE TABLE author;``

  .. code-block:: psql

    postgres=# TRUNCATE TABLE author;
    TRUNCATE TABLE
