9 - Postgres Administration
===========================
This chapter covers some of the frequently used ``psql`` commands. You can
check all the ``psql`` commands by typing ``\?``.


9.1 How to Turn on Query Timing
-------------------------------
**\\timing**
  By default, the timing command is set to off. Enabling this will display how
  long it takes for the query to run, it is measured in milliseconds.

  .. code-block:: psql

    test_database=> \timing
    Timing is on.
    test_database=> \timing
    Timing is off.
    test_database=> \timing
    Timing is on.
    test_database=> test_database=> SELECT * FROM author ORDER BY id;
     id | first_name | last_name |      email
    ----+------------+-----------+------------------
      1 | Jon        | Doe       |
      2 | Kevin      | Mallone   |
      3 | Jane       | Doe       |
      4 | Robb       | Stark     | robb@stark.com
      5 | Sansa      | Stark     | sansa@stark.com
      6 | Eddard     | Stark     | ned@stark.com
      7 | Stark      | Stark     | arya@stark.com
      8 | Arya       | Stark     | arya@stark.com
      9 | Bran       | Stark     | bran@stark.com
     10 | Rickon

    Time: 0.634 ms


9.2 How to List down Tables, Databases, and Schemas
---------------------------------------------------
**\\dt**
  This command is used to list down tables in a database.

  .. code-block:: psql

    test_database=> \dt
              List of relations
     Schema |  Name  | Type  |   Owner
    --------+--------+-------+-----------
     public | author | table | test_user
     public | book   | table | test_user
    (2 rows)

_______________________________________________________________________________

**\\d table_name**
  This command is used to describe a table, it shows all the data types and
  constraints.

  .. code-block:: psql

    test_database=> \d author
                                         Table "public.author"
       Column   |         Type          | Collation | Nullable |              Default
    ------------+-----------------------+-----------+----------+------------------------------------
     id         | bigint                |           | not null | nextval('author_id_seq'::regclass)
     first_name | character varying(50) |           | not null |
     last_name  | character varying(50) |           | not null |
     email      | character varying(50) |           |          |
    Indexes:
        "author_pkey" PRIMARY KEY, btree (id)
    Referenced by:
        TABLE "book" CONSTRAINT "book_author_id_fkey" FOREIGN KEY (author_id) REFERENCES author(id)


_______________________________________________________________________________

**\\l**
  This command is used to list down all existing databases.

  .. code-block:: psql

    test_database=> test_database=> \l
                                    List of databases
         Name      |  Owner   | Encoding | Collate |  Ctype  |   Access privileges
    ---------------+----------+----------+---------+---------+------------------------
     bedweiser_db  | postgres | UTF8     | C.UTF-8 | C.UTF-8 | =Tc/postgres          +
                   |          |          |         |         | postgres=CTc/postgres +
                   |          |          |         |         | novostorm=CTc/postgres
     postgres      | postgres | UTF8     | C.UTF-8 | C.UTF-8 |
     template0     | postgres | UTF8     | C.UTF-8 | C.UTF-8 | =c/postgres           +
                   |          |          |         |         | postgres=CTc/postgres
     template1     | postgres | UTF8     | C.UTF-8 | C.UTF-8 | =c/postgres           +
                   |          |          |         |         | postgres=CTc/postgres
     test_database | postgres | UTF8     | C.UTF-8 | C.UTF-8 | =Tc/postgres          +
                   |          |          |         |         | postgres=CTc/postgres +
                   |          |          |         |         | test_user=CTc/postgres
    (5 rows)


_______________________________________________________________________________

**\\dn**
  This command is used to list all schemas. We will be discussing more about
  schemas later on.

  .. code-block:: psql

    test_database=> \dn
      List of schemas
      Name  |  Owner
    --------+----------
     public | postgres
    (1 row)


.. note:: You can add the plus sign ``+`` to view additional information, for
          example: ``\dt+`` or ``\l+``

