6 - Aggregate Functions and Grouping Data
=========================================
This chapter focuses on some of the commonly used aggregate functions and how
to use them with ``GROUP BY`` statement.

Book Table:

+----+------------+-----------+--------+
| id | title      | author_id |  price |
+====+============+===========+========+
|  1 | Harry Pot  | 1         | 400    |
+----+------------+-----------+--------+
|  2 | Potter Har | 1         | 500    |
+----+------------+-----------+--------+
|  3 | The Thing  | 2         | 600    |
+----+------------+-----------+--------+
|  4 | The Car    | 3         | 700    |
+----+------------+-----------+--------+
|  5 | The Wizard | 4         | 800    |
+----+------------+-----------+--------+

Author Table:

+----+------------+-----------+------------+--------+
| id | first_name | last_name |  birthday  | gender |
+====+============+===========+============+========+
|  1 | John       | Doe       |            | male   |
+----+------------+-----------+------------+--------+
|  2 | Jane       | Doe       | 1996-07-23 | female |
+----+------------+-----------+------------+--------+
|  3 | Jane       | Foster    | 1988-06-23 | female |
+----+------------+-----------+------------+--------+
|  4 | John       | Foster    | 1990-04-23 | male   |
+----+------------+-----------+------------+--------+
|  5 | John       | Snow      | 1990-02-13 | male   |
+----+------------+-----------+------------+--------+


6.1 Aggregate Functions
-----------------------
Aggregate functions perform calculations on a set of values and return a single
value. The following are the frequently used SQL aggregate functions:

**MIN**
  This function is used to return the smallest value of the specified column.
  The value can either be a number, word, date, etc.

    syntax: ``SELECT MIN(column_name) FROM table_name WHERE condition``

    example: ``SELECT MIN(price) FROM book WHERE author_id = 1;``

  .. code-block:: psql

    postgres=# SELECT MIN(price) FROM book WHERE author_id = 1;
     min
    -----
     400
    (1 row)

_______________________________________________________________________________

**MAX**
  This function is the counterpart of ``MIN``, it returns the biggest value of
  the specified column.

    syntax: ``SELECT MAX(column_name) FROM table_name WHERE condition``

    example: ``SELECT MAX(price) FROM book WHERE author_id = 1;``

  .. code-block:: psql

    postgres=# SELECT MAX(price) FROM book WHERE author_id = 1;
     max
    -----
     500
    (1 row)


_______________________________________________________________________________

**COUNT**
  This function counts the number of rows that matches the specified criteria.

    syntax: ``SELECT COUNT(column_name) FROM table_name WHERE condition``

    example: ``SELECT COUNT(price) FROM book WHERE author_id = 1;``

  .. code-block:: psql

    postgres=# SELECT COUNT(price) FROM book WHERE author_id = 1;
     count
    -------
         2
    (1 row)


_______________________________________________________________________________

**AVG**
  This function computes for the average value of a numeric column. It will
  not work on a non numeric data.

    syntax: ``SELECT AVG(column_name) FROM table_name WHERE condition``

    example: ``SELECT AVG(price) FROM book WHERE author_id = 1;``

  .. code-block:: psql

    postgres=# SELECT AVG(price) FROM book WHERE author_id = 1;
             avg
    ----------------------
     450.0000000000000000
    (1 row)


_______________________________________________________________________________

**SUM**
  This function is used to return the sum of the specified numeric column.

    syntax: ``SELECT SUM(column_name) FROM table_name WHERE condition``

    example: ``SELECT SUM(price) FROM book WHERE author_id = 1;``

  .. code-block:: psql

    postgres=# SELECT SUM(price) FROM book WHERE author_id = 1;
     sum
    -----
     900
    (1 row)


6.2 Group Data with the Same Values
-----------------------------------
**GROUP BY**
  This statement groups rows that have the same values and is often used with
  aggregate functions.

    syntax: ``SELECT column_name/s FROM table_name WHERE condition GROUP BY column_name/s ORDER BY column_name/s``

    example: ``SELECT last_name, COUNT(*) FROM author GROUP BY last_name ORDER BY COUNT(*) ASC;``

  .. code-block:: psql

    postgres=# SELECT last_name, COUNT(*) FROM author GROUP BY last_name ORDER BY COUNT(*) ASC;
     last_name | count
    -----------+-------
     Snow      |     1
     Doe       |     2
     Foster    |     2
    (3 rows)


6.3 Add Conditions with Aggregate Functions
-------------------------------------------
**HAVING**
  This clause was added because ``WHERE`` keyword does not work with aggregate
  functions.

    syntax: ``SELECT column_name/s FROM table_name WHERE condition GROUP BY column_name/s HAVING condition ORDER BY column_name/s``

    example: ``SELECT last_name, COUNT(*) FROM author GROUP BY last_name HAVING COUNT(*) = 2 ORDER BY COUNT(*) ASC;``

  .. code-block:: psql

    postgres=# SELECT last_name, COUNT(*) FROM author GROUP BY last_name HAVING COUNT(*) = 2 ORDER BY COUNT(*) ASC;
     last_name | count
    -----------+-------
     Doe       |     2
     Foster    |     2
    (2 rows)


  In the example above we filtered the data to have a ``COUNT(*)`` equal to 2.

