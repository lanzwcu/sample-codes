5 - Filtering Data
==================
This chapter focuses on additional ways to filter data.

Author Table:

+----+------------+-----------+------------+--------+
| id | first_name | last_name |  birthday  | gender |
+====+============+===========+============+========+
|  1 | John       | Doe       |            | male   |
+----+------------+-----------+------------+--------+
|  2 | Jane       | Doe       | 1996-07-23 | female |
+----+------------+-----------+------------+--------+
|  3 | Jane       | Foster    | 1988-06-23 | female |
+----+------------+-----------+------------+--------+
|  4 | John       | Foster    | 1990-04-23 | male   |
+----+------------+-----------+------------+--------+
|  5 | John       | Snow      | 1990-02-13 | male   |
+----+------------+-----------+------------+--------+


5.1 Basic Filtering Operators
-----------------------------
These operators are used to filter records based on condition/s, it is commonly
combined with ``WHERE`` clause.

**AND**
  This operator requires both conditions to be satisfied:

    example: ``SELECT * from author WHERE last_name = 'Doe' AND first_name = 'John';``

  .. code-block:: psql

    postgres=# SELECT * from author WHERE last_name = 'Doe' AND first_name = 'John';
     id | first_name | last_name | birthday | gender
    ----+------------+-----------+----------+--------
      1 | John       | Doe       |          | male
    (1 row)


_______________________________________________________________________________

**OR**
  This operator requires either one of the conditions to be satisfied:

    example: ``SELECT * from author WHERE last_name = 'Doe' OR first_name = 'John';``

  .. code-block:: psql

    postgres=# SELECT * from author WHERE last_name = 'Doe' OR first_name = 'John';
     id | first_name | last_name |  birthday  | gender
    ----+------------+-----------+------------+--------
      1 | John       | Doe       |            | male
      2 | Jane       | Doe       | 1996-07-23 | female
      4 | John       | Foster    | 1990-04-23 | male
      5 | John       | Snow      | 1990-02-13 | male
    (4 rows)


_______________________________________________________________________________

**NOT**
  This operator filters which is not in the specified condition:

    example: ``SELECT * from author WHERE NOT last_name = 'Doe';``

  .. code-block:: psql

    postgres=# SELECT * from author WHERE NOT last_name = 'Doe';
     id | first_name | last_name |  birthday  | gender
    ----+------------+-----------+------------+--------
      3 | Jane       | Foster    | 1988-06-23 | female
      4 | John       | Foster    | 1990-04-23 | male
      5 | John       | Snow      | 1990-02-13 | male
    (3 rows)


5.2 Filter Rows Returned
------------------------
**LIMIT**
  This keyword is used to limit the rows returned as specified.

    example: ``SELECT * FROM author ORDER BY last_name LIMIT 4;``

  .. code-block:: psql

    postgres=# SELECT * FROM author ORDER BY last_name LIMIT 4;
     id | first_name | last_name |  birthday  | gender
    ----+------------+-----------+------------+--------
      2 | Jane       | Doe       | 1996-07-23 | female
      1 | John       | Doe       |            | male
      3 | Jane       | Foster    | 1988-06-23 | female
      4 | John       | Foster    | 1990-04-23 | male
    (4 rows)


_______________________________________________________________________________

**OFFSET**
  This keyword will start the rows returned from the number specified.

    example: ``SELECT * FROM author ORDER BY last_name OFFSET 1 LIMIT 3;``

  .. code-block:: psql

    postgres=# SELECT * FROM author ORDER BY last_name OFFSET 1 LIMIT 3;
     id | first_name | last_name |  birthday  | gender
    ----+------------+-----------+------------+--------
      1 | John       | Doe       |            | male
      3 | Jane       | Foster    | 1988-06-23 | female
      4 | John       | Foster    | 1990-04-23 | male
    (3 rows)


5.3 Filter with a Given Range
-----------------------------
**BETWEEN**
  This keyword is used to select data from a given range.

    syntax: ``SELECT column_name/s FROM table_name WHERE column_name BETWEEN value1 AND value2``

    example: ``SELECT * FROM author WHERE birthday BETWEEN '1985-01-01' AND '1991-01-01';``

  .. code-block:: psql

    postgres=# SELECT * FROM author WHERE birthday BETWEEN '1985-01-01' AND '1991-01-01';
     id | first_name | last_name |  birthday  | gender
    ----+------------+-----------+------------+--------
      3 | Jane       | Foster    | 1988-06-23 | female
      4 | John       | Foster    | 1990-04-23 | male
      5 | John       | Snow      | 1990-02-13 | male
    (3 rows)


5.4 Filter with a Specific Pattern
----------------------------------
**LIKE**
  This operator is used in a ``WHERE`` clause to match values against a case
  sensitive pattern. The 2 wildcards commonly used with this operator are:

    - ``%`` - represents 0, 1 , or multiple characters
    - ``_`` - represents a single character

  The example below returns all rows with last names that end with 'oe'.

    syntax: ``SELECT column_name/s FROM table_name WHERE column_name LIKe pattern``

    example 1: ``SELECT * FROM author WHERE last_name LIKE '%oe';``

  .. code-block:: psql

    postgres=# SELECT * FROM author WHERE last_name LIKE '%oe';
     id | first_name | last_name |  birthday  | gender
    ----+------------+-----------+------------+--------
      2 | Jane       | Doe       | 1996-07-23 | female
      1 | John       | Doe       |            | male
    (2 rows)


_______________________________________________________________________________

  If we put the wildcard ``%`` at the end, it will return all rows with last names
  that start with 'D'.

    example 2: ``SELECT * FROM author WHERE last_name LIKE 'F%';``

  .. code-block:: psql

    postgres=# SELECT * FROM author WHERE last_name LIKE 'F%';
     id | first_name | last_name |  birthday  | gender
    ----+------------+-----------+------------+--------
      3 | Jane       | Foster    | 1988-06-23 | female
      4 | John       | Foster    | 1990-04-23 | male
    (2 rows)


_______________________________________________________________________________

  We can also use it to find values in the middle of a string:

    example 3: ``SELECT * FROM author WHERE last_name LIKE '%ost%';``

  .. code-block:: psql

    postgres=# SELECT * FROM author WHERE last_name LIKE '%ost%';
     id | first_name | last_name |  birthday  | gender
    ----+------------+-----------+------------+--------
      3 | Jane       | Foster    | 1988-06-23 | female
      4 | John       | Foster    | 1990-04-23 | male
    (2 rows)


_______________________________________________________________________________

  The ``_`` as mentioned above, represents a single character. Let's look for
  a pattern like ``F _ _ t %``

  example 4: ``SELECT * FROM author WHERE last_name LIKE 'F__t%';``

  .. code-block:: psql

    postgres=# SELECT * FROM author WHERE last_name LIKE 'F__t%';
     id | first_name | last_name |  birthday  | gender
    ----+------------+-----------+------------+--------
      3 | Jane       | Foster    | 1988-06-23 | female
      4 | John       | Foster    | 1990-04-23 | male
    (2 rows)


  If we remove one underscore from that query, we will not get any results:

  .. code-block:: psql

    postgres=# SELECT * FROM author WHERE last_name LIKE 'F_t%';
     id | first_name | last_name | birthday | gender
    ----+------------+-----------+----------+--------
    (0 rows)


_______________________________________________________________________________

**ILIKE**
  This operator operates the same way as ``LIKE``, except that it is not case
  sensitive.

  example: ``SELECT * FROM author WHERE last_name ILIKE 'f__t%';``

  .. code-block:: psql

    postgres=# SELECT * FROM author WHERE last_name ILIKE 'f__t%';
     id | first_name | last_name |  birthday  | gender
    ----+------------+-----------+------------+--------
      3 | Jane       | Foster    | 1988-06-23 | female
      4 | John       | Foster    | 1990-04-23 | male
    (2 rows)

    postgres=# SELECT * FROM author WHERE last_name LIKE 'f__t%';
     id | first_name | last_name | birthday | gender
    ----+------------+-----------+----------+--------
    (0 rows)



5.5 Filter with Multiple Values
-------------------------------
**IN**
  This operator allows you to specify multiple conditions in a ``WHERE`` clause.
  It returns rows matching any of the conditions specified.

    syntax: ``SELECT column_name/s FROM table_name WHERE column_name IN (value/s)``

    example: ``SELECT * FROM author WHERE last_name IN ('Doe', 'Foster') ORDER BY first_name;``

  .. code-block:: psql

    postgres=# SELECT * FROM author WHERE last_name IN ('Doe', 'Foster') ORDER BY first_name;
     id | first_name | last_name |  birthday  | gender
    ----+------------+-----------+------------+--------
      2 | Jane       | Doe       | 1996-07-23 | female
      3 | Jane       | Foster    | 1988-06-23 | female
      4 | John       | Foster    | 1990-04-23 | male
      1 | John       | Doe       |            | male
    (4 rows)



