7 - Joining Tables
==================
This chapter focuses on relating tables to each other. A ``FOREIGN KEY`` is used
to connect two or more tables together, it is a ``PRIMARY KEY`` in another
table. To create a relationship, we have to reference them like so:

.. code-block:: psql

  postgres=# CREATE TABLE book (
  id BIGSERIAL NOT NULL PRIMARY KEY,
  title VARCHAR(50) NOT NULL,
  author_id BIGINT NOT NULL REFERENCES author (id),
  price int NOT NULL
  );
  CREATE TABLE


The following tables will serve as demo tables:

``author`` table:

+----+------------+-----------+------------+--------+
| id | first_name | last_name |  birthday  | gender |
+====+============+===========+============+========+
|  1 | John       | Doe       |            | male   |
+----+------------+-----------+------------+--------+
|  2 | Jane       | Doe       | 1996-07-23 | female |
+----+------------+-----------+------------+--------+
|  3 | Jane       | Foster    | 1988-06-23 | female |
+----+------------+-----------+------------+--------+
|  4 | John       | Foster    | 1990-04-23 | male   |
+----+------------+-----------+------------+--------+
|  5 | John       | Snow      | 1990-02-13 | male   |
+----+------------+-----------+------------+--------+

``book`` table:

+----+------------+-----------+--------+
| id | title      | author_id |  price |
+====+============+===========+========+
|  1 | Harry Pot  | 1         | 400    |
+----+------------+-----------+--------+
|  2 | Potter Har | 1         | 500    |
+----+------------+-----------+--------+
|  3 | The Thing  | 2         | 600    |
+----+------------+-----------+--------+
|  4 | The Car    | 3         | 700    |
+----+------------+-----------+--------+
|  5 | The Wizard | 4         | 800    |
+----+------------+-----------+--------+
|  6 | The Nun    |           |  1000  |
+----+------------+-----------+--------+

.. note:: The key factors in this dummy data are the following:

          - The author ``John Snow`` has no record under table ``book``.
          - The book ``The Nun`` has no ``author_id`` specified.


7.1 Join Clause
---------------
A ``JOIN`` clause is used to combine rows from 2 or more tables using the related
column.

In the demo tables above, we have a column named ``author_id`` under the ``book``
table. This refers to the ``id`` of the ``author`` table. In other words, ``author.id``
is equivalent to ``book.author_id``.

Having this kind of relationship enables us to fetch data from either of the two
tables using the related values.

The different types of ``JOINS`` are:

- (INNER) JOIN
- LEFT (OUTER) JOIN
- RIGHT (OUTER) JOIN
- FULL (OUTER) JOIN

We will be using the same query (altering only the keyword) as examples for
all the types of ``JOINS`` to have a better understanding. The examples below uses
table1 for ``author`` and table2 for ``book``.


7.2 Inner Joins
---------------
**INNER JOINS**
  This keyword is used to select records that have matching values on both tables.

    syntax: ``SELECT column_name/s FROM table1 INNER JOIN table2 ON table1.column_name = table2.column_name``

    example: ``SELECT * FROM author INNER JOIN book ON author.id = book.author_id ORDER BY author_id ASC;``

  .. code-block:: psql

    postgres=# SELECT * FROM author INNER JOIN book ON author.id = book.author_id ORDER BY author_id ASC;
     id | first_name | last_name |  birthday  | gender | id |   title    | author_id | price
    ----+------------+-----------+------------+--------+----+------------+-----------+-------
      1 | John       | Doe       |            | male   |  2 | Potter Har |         1 |   500
      1 | John       | Doe       |            | male   |  1 | Harry Pot  |         1 |   400
      2 | Jane       | Doe       | 1996-07-23 | female |  3 | The Thing  |         2 |   600
      3 | Jane       | Foster    | 1988-06-23 | female |  4 | The Car    |         3 |   700
      4 | John       | Foster    | 1990-04-23 | male   |  5 | The Wizard |         4 |   800
    (5 rows)


  Since this keyword only returns records that have matching values on both tables,
  it will only return 5 rows because the ``id`` of author ``John Snow`` cannot be
  found under the ``book`` table. It will also not return the book ``The Nun``
  because it does not have an ``author_id``.


7.3 Left Joins
--------------
**LEFT JOIN**
  This keyword is used to select all records from table1 and the matched values
  from table2. If there is no match, it will return ``NULL``

    syntax: ``SELECT column_name/s FROM table1 LEFT JOIN table2 ON table1.column_name = table2.column_name``

    example: ``SELECT * FROM author LEFT JOIN book ON author.id = book.author_id ORDER BY author_id;``

  .. code-block:: psql

    postgres=# SELECT * FROM author LEFT JOIN book ON author.id = book.author_id ORDER BY author_id;
     id | first_name | last_name |  birthday  | gender | id |   title    | author_id | price
    ----+------------+-----------+------------+--------+----+------------+-----------+-------
      1 | John       | Doe       |            | male   |  2 | Potter Har |         1 |   500
      1 | John       | Doe       |            | male   |  1 | Harry Pot  |         1 |   400
      2 | Jane       | Doe       | 1996-07-23 | female |  3 | The Thing  |         2 |   600
      3 | Jane       | Foster    | 1988-06-23 | female |  4 | The Car    |         3 |   700
      4 | John       | Foster    | 1990-04-23 | male   |  5 | The Wizard |         4 |   800
      5 | John       | Snow      | 1990-02-13 | male   |    |            |           |
    (6 rows)


  This example returns 6 rows because ``LEFT JOIN`` returns all records from table1
  and returns ``NULL`` if there is no match. On the last row we can see that it returns
  ``John Snow`` having ``NULL`` values for ``book`` columns. It will  not return the
  book ``The Nun`` because it does not have an ``author_id`` and is located under table2.


7.4 Right Joins
---------------
**RIGHT JOIN**
  This keyword is used to select all records from table2 and the matched values
  from table1. If there is no match, it will return ``NULL``

    syntax: ``SELECT column_name/s FROM table1 RIGHT JOIN table2 ON table1.column_name = table2.column_name``

    example: ``SELECT * FROM author RIGHT JOIN book ON author.id = book.author_id;``

  .. code-block:: psql

    postgres=# SELECT * FROM author RIGHT JOIN book ON author.id = book.author_id ORDER BY author_id;
     id | first_name | last_name |  birthday  | gender | id |   title    | author_id | price
    ----+------------+-----------+------------+--------+----+------------+-----------+-------
      1 | John       | Doe       |            | male   |  2 | Potter Har |         1 |   500
      1 | John       | Doe       |            | male   |  1 | Harry Pot  |         1 |   400
      2 | Jane       | Doe       | 1996-07-23 | female |  3 | The Thing  |         2 |   600
      3 | Jane       | Foster    | 1988-06-23 | female |  4 | The Car    |         3 |   700
      4 | John       | Foster    | 1990-04-23 | male   |  5 | The Wizard |         4 |   800
        |            |           |            |        |  6 | The Nun    |           |  1000
    (6 rows)


  The ``RIGHT JOIN`` is the opposite of ``LEFT JOIN``, meaning it will only return
  records from table2 that has a match. Since ``John Snow``'s ``id`` cannot be
  found in ``book`` table, it does not return it. But it returns the book ``The Nun``
  since it can be found in the table2.


7.5 Full Joins
--------------
**FULL JOIN**
  This keyword is used to select all records when there is a match.

    syntax: ``SELECT column_name/s FROM table1 FULL JOIN table2 ON table1.column_name = table2.column_name``

    example: ``SELECT * FROM author FULL JOIN book ON author.id = book.author_id ORDER BY author_id;``

  .. code-block:: psql

    postgres=# SELECT * FROM author FULL JOIN book ON author.id = book.author_id ORDER BY author_id;
     id | first_name | last_name |  birthday  | gender | id |   title    | author_id | price
    ----+------------+-----------+------------+--------+----+------------+-----------+-------
      1 | John       | Doe       |            | male   |  1 | Harry Pot  |         1 |   400
      1 | John       | Doe       |            | male   |  2 | Potter Har |         1 |   500
      2 | Jane       | Doe       | 1996-07-23 | female |  3 | The Thing  |         2 |   600
      3 | Jane       | Foster    | 1988-06-23 | female |  4 | The Car    |         3 |   700
      4 | John       | Foster    | 1990-04-23 | male   |  5 | The Wizard |         4 |   800
      5 | John       | Snow      | 1990-02-13 | male   |    |            |           |
        |            |           |            |        |  6 | The Nun    |           |  1000
    (7 rows)


  ``FULL JOIN`` returns all records from both tables and returns ``NULL`` values
  if those records does not exist in the other table. Here we can see both author
  ``John Snow`` and book ``The Nun``.

