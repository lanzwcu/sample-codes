3 - Constraints
---------------
Constraints are used for validation to specify specific rules for a data type.
It is declared beside the data type in a ``CREATE TABLE`` statement, like this:

.. code-block:: psql

  postgres=# CREATE TABLE author (
  id BIGSERIAL NOT NULL PRIMARY KEY,
  first_name VARCHAR(50) NOT NULL,
  last_name VARCHAR(50) NOT NULL,
  birthday date,
  gender varchar(6) NOT NULL
  );
  CREATE TABLE


_______________________________________________________________________________

Below are some of the frequently used constraints:

**PRIMARY KEY**
  Primary keys are unique values and cannot be NULL, it uniquely identifies
  each record in a table. There can only be 1 Primary key per table.

**FOREIGN KEY**
  Foreign keys are used to link two tables together, it is a field in one table
  that is equivalent to the primary key of another table.

**NOT NULL**
  Columns can hold NULL values by default, adding this constraint will not allow
  NULL values.

**UNIQUE**
  This constraint is used to make sure that all values in a column are unique.

**DEFAULT**
  This constraint is used to provide default values for a column, the default
  value is automatically set if no other value is specified.


