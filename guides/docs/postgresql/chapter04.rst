4 - Basic SQL Commands
======================
This chapter focuses on some of the frequently used SQL commands.

``author`` table:

+----+------------+-----------+------------+--------+
| id | first_name | last_name |  birthday  | gender |
+====+============+===========+============+========+
|  1 | John       | Doe       |            | male   |
+----+------------+-----------+------------+--------+
|  2 | Jane       | Doe       | 1996-07-23 | female |
+----+------------+-----------+------------+--------+
|  3 | Jane       | Foster    | 1988-06-23 | female |
+----+------------+-----------+------------+--------+
|  4 | John       | Foster    | 1990-04-23 | male   |
+----+------------+-----------+------------+--------+
|  5 | John       | Snow      | 1990-02-13 | male   |
+----+------------+-----------+------------+--------+

``writer`` table:

+----+------------+-----------+------------+--------+---------------+
| id | first_name | last_name |  birthday  | gender | books_written |
+====+============+===========+============+========+===============+
|  1 | Aaron      | Test      | 1976-04-13 | male   | 5             |
+----+------------+-----------+------------+--------+---------------+
|  2 | Buck       | Test      | 1998-12-31 | male   | 8             |
+----+------------+-----------+------------+--------+---------------+
|  3 | Candice    | Test      | 1993-08-06 | female | 16            |
+----+------------+-----------+------------+--------+---------------+


4.1 Insert Into a Table
-----------------------
**INSERT INTO**
  This statement is used to insert new record/s in an existing table.

    syntax 1: ``INSERT INTO table_name (column/s) VALUES ('value/s')``

    example 1.1: ``INSERT INTO author (first_name, last_name, gender) VALUES ('Test', 'Doe','male');``

  .. code-block:: psql

    postgres=# INSERT INTO author (first_name, last_name, gender) VALUES ('Test', 'Doe','male');
    INSERT 0 1


_______________________________________________________________________________

  To insert multiple rows, all we have to do is separate each row with a comma:

    example 1.2: ``INSERT INTO author (id, first_name, last_name, birthday, gender)
    VALUES ('6', 'Jane', 'Test', '1996-07-23', 'female'), ('7', 'Jack', 'Test',
    '1996-07-23', 'male'), ('8', 'Jerry', 'Test', '1996-07-23', 'male');``

  .. code-block:: psql

    postgres=# INSERT INTO author (id, first_name, last_name, birthday, gender)
    VALUES ('6', 'Jane', 'Test', '1996-07-23', 'female'),
    ('7', 'Jack', 'Test', '1996-07-23', 'male'),
    ('8', 'Jerry', 'Test', '1996-07-23', 'male');
    INSERT 0 3


_______________________________________________________________________________

  To insert multiple rows from another table with a condition using a
  ``SELECT`` statement:

    syntax 2: ``INSERT INTO table1 (table1_column_names/s) SELECT table2_column_name/s FROM table2 WHERE condition``

    example 2: ``INSERT INTO author (first_name, last_name, birthday, gender)
    SELECT first_name, last_name, birthday, gender FROM writer WHERE gender = 'male';``

  .. code-block:: psql

    postgres=# INSERT INTO author (first_name, last_name, birthday, gender) SELECT first_name, last_name, birthday, gender FROM writer WHERE gender = 'male';
    INSERT 0 2


4.2 Select Records
------------------
**SELECT**
  This command is used to select specific item/s from a table.

    syntax: ``SELECT column_name/s FROM table_name``

    example 1: ``SELECT last_name FROM author;``

  .. code-block:: psql

    postgres=# SELECT last_name FROM author;
     last_name
    -----------
     Doe
     Doe
     Foster
     Foster
     Snow

_______________________________________________________________________________

  To select all columns, we use ``*``:

    example 2: ``SELECT * FROM author;``

  .. code-block:: psql

    postgres=# SELECT * FROM author;
     id | first_name | last_name |  birthday  | gender
    ----+------------+-----------+------------+--------
      1 | John       | Doe       |            | male
      2 | Jane       | Doe       | 1996-07-23 | female
      3 | Jane       | Foster    | 1988-06-23 | female
      4 | John       | Foster    | 1990-04-23 | male
      5 | John       | Snow      | 1990-02-13 | male
    (5 rows)


_______________________________________________________________________________

**SELECT DISTINCT**
  This statement is used to filter out duplicate values in a given query.

    syntax: ``SELECT DISTINCT column_name/s FROM table_name``

    example: ``SELECT DISTINCT last_name FROM author;``

  .. code-block:: psql

    postgres=# SELECT DISTINCT last_name FROM author;
     last_name
    -----------
     Doe
     Snow
     Foster
    (3 rows)


4.3 Filter Records
------------------
**WHERE**
  This keyword is used to filter the query. ``WHERE`` may be used in
  ``SELECT``, ``UPDATE``, ``DELETE``, etc.

    syntax: ``SELECT column_name/s FROM table_name WHERE column_name = 'value'``

    example: ``SELECT * from author WHERE last_name = 'Doe';``

  .. code-block:: psql

    postgres=# SELECT * from author WHERE last_name = 'Doe';
     id | first_name | last_name |  birthday  | gender
    ----+------------+-----------+------------+--------
      1 | John       | Doe       |            | male
      2 | Jane       | Doe       | 1996-07-23 | female
    (2 rows)


  There are several operators we can used in the ``WHERE`` clause, here are some:

  - ``=`` - Equal
  - ``>`` - Greater than
  - ``<`` - Less than
  - ``>=`` - Greater than or equal to
  - ``<=`` - Less than or equal to
  - ``BETWEEN`` - Between a specified range
  - ``LIKE``/``ILIKE`` - search for a pattern
  - ``IN`` - To specify multiple values for a column

  ``Between``, ``LIKE`` and ``IN`` will be discussed in detail in the next chapter.


4.4 Sort Records
----------------
**ORDER BY**
  This keyword takes a column and orders the data by ascending (ASC) or
  descending (DESC) order. If ``ASC`` or ``DESC`` is not specified when using
  ``ORDER BY``, it sorts in ascending order by default.

    syntax: ``SELECT column_name/s FROM table_name ORDER BY column_name/a ASC/DESC``

    example 1: ``SELECT * FROM author ORDER BY last_name;``

  .. code-block:: psql

    postgres=# SELECT * FROM author ORDER BY last_name;
     id | first_name | last_name |  birthday  | gender
    ----+------------+-----------+------------+--------
      1 | John       | Doe       |            | male
      2 | Jane       | Doe       | 1996-07-23 | female
      4 | John       | Foster    | 1990-04-23 | male
      3 | Jane       | Foster    | 1988-06-23 | female
      5 | John       | Snow      | 1990-02-13 | male
    (5 rows)


_______________________________________________________________________________

  Let's put in ``DESC``:

    example 2: ``SELECT * FROM author ORDER BY last_name DESC;``

  .. code-block:: psql

    postgres=# SELECT * FROM author ORDER BY last_name DESC;
     id | first_name | last_name |  birthday  | gender
    ----+------------+-----------+------------+--------
      5 | John       | Snow      | 1990-02-13 | male
      4 | John       | Foster    | 1990-04-23 | male
      3 | Jane       | Foster    | 1988-06-23 | female
      1 | John       | Doe       |            | male
      2 | Jane       | Doe       | 1996-07-23 | female
    (5 rows)


4.5 Update Records
------------------
**UPDATE**
  This statement is used to modify existing record/s in a table.

      syntax: ``UPDATE table_name SET column1 = value1, column2 = value2 WHERE condition``

      example: ``UPDATE author SET birthday = '1984-12-12' WHERE id = 1;``

  .. code-block:: psql

    postgres=# UPDATE author SET birthday = '1984-12-12' WHERE id = 1;
    UPDATE 1


.. warning:: Be mindful when using ``UPDATE``, you might accidentally change
             rows that don't need to be changed if the ``WHERE`` clause is
             incorrect. The only instance when you should remove the ``WHERE`` clause
             is when you want all rows to be affected by the change.


4.6 Delete Records
------------------
**DELETE**
  This statement is used to delete existing records in a table.

      syntax: ``DELETE FROM table_name WHERE condition``

      example: ``DELETE FROM author WHERE first_name = 'Jon' AND last_name = 'Snow';``

  .. code-block:: psql

    postgres=# DELETE FROM author WHERE first_name = 'Jon' AND last_name = 'Snow';
    DELETE 1


.. warning:: Be careful when using ``DELETE``, make sure that your ``WHERE``
             statements are correct and not omitted otherwise it will affect the whole table.
             The only instance when you should remove the ``WHERE`` clause
             is when you want to delete all records from the table.


