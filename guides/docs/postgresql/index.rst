.. PostgreSQL basics documentation master file, created by
   sphinx-quickstart on Tue Sep 24 10:24:05 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PostgreSQL Cheatsheet
=====================

.. image:: /images/postgres_banner.jpg

PostgreSQL or commonly known as Postgres, is an open source relational database
management system. Postgres uses SQL as its main query language, SQL stands for
"structured query language" and we will be discussing some of its frequently
used commands.

.. note:: This document assumes you already have postgres installed.


.. toctree::
   :maxdepth: 2

   chapter01
   chapter02
   chapter03
   chapter04
   chapter05
   chapter06
   chapter07
   chapter08
   chapter09
   chapter10