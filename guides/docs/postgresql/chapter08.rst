8 - Subqueries and Aliases
==========================
This chapter covers subqueries and aliases.

8.1 Subqueries
--------------
A subquery is basically a query that lies inside another query. Subqueries are
enclosed in parenthesis to make them easier to spot. It is often referred to as
the inner query and the main query is referred to as the outer query.

A subquery is used when we need to query data that is returned by another query.
The are a couple of rules that a subquery must follow:

- Subqueries must be enclosed in parenthesis.
- A subquery can only return one column in a ``SELECT`` clause, unless multiple columns are in the main query for the subquery to compare its selected columns.
- Subqueries that return more than one row can only be used with multiple value operators like ``IN``.
- You can use comparison operators such as ``>`` , ``<``, ``=``, ``BETWEEN``, ``LIKE``, etc.


The following tables will serve as demo tables:

``author`` table:

+----+------------+-----------+------------+--------+
| id | first_name | last_name |  birthday  | gender |
+====+============+===========+============+========+
|  1 | John       | Doe       |            | male   |
+----+------------+-----------+------------+--------+
|  2 | Jane       | Doe       | 1996-07-23 | female |
+----+------------+-----------+------------+--------+
|  3 | Jane       | Foster    | 1988-06-23 | female |
+----+------------+-----------+------------+--------+
|  4 | John       | Foster    | 1990-04-23 | male   |
+----+------------+-----------+------------+--------+
|  5 | John       | Snow      | 1990-02-13 | male   |
+----+------------+-----------+------------+--------+

``book`` table:

+----+------------+-----------+--------+
| id | title      | author_id |  price |
+====+============+===========+========+
|  1 | Harry Pot  | 1         | 400    |
+----+------------+-----------+--------+
|  2 | Potter Har | 1         | 500    |
+----+------------+-----------+--------+
|  3 | The Thing  | 2         | 600    |
+----+------------+-----------+--------+
|  4 | The Car    | 3         | 700    |
+----+------------+-----------+--------+
|  5 | The Wizard | 4         | 800    |
+----+------------+-----------+--------+

Suppose we want to retrieve authors' names that authored a book beginning with
'The' in the title.

Let's create the inner query first that returns the ``author_id`` of books that
start with 'The':

  ``SELECT author_id FROM book WHERE title LIKE 'Song%';``

  .. code-block:: psql

    postgres=# SELECT author_id FROM book WHERE title LIKE 'The%';
     author_id
    -----------
             2
             3
             4
    (3 rows)


Then for the outer query, we fetch the first and last names of these authors
using the ``author_id`` from the query earlier:

  ``SELECT first_name, last_name FROM author WHERE id IN (2, 3, 4);``

  .. code-block:: psql

    postgres=# SELECT first_name, last_name FROM author WHERE id IN (2, 3, 4);
     first_name | last_name
    ------------+-----------
     Jane       | Doe
     Jane       | Foster
     John       | Foster
    (3 rows)


Now let's put the inner query in a parenthesis and replace the ``(2, 3, 4)``
in our outer query with it:

  ``SELECT first_name, last_name FROM author WHERE id IN (SELECT author_id FROM book WHERE title LIKE 'The%');``

  .. code-block:: psql

    postgres=# SELECT first_name, last_name FROM author WHERE id IN (SELECT author_id FROM book WHERE title LIKE 'The%');
     first_name | last_name
    ------------+-----------
     Jane       | Doe
     Jane       | Foster
     John       | Foster
    (3 rows)

_______________________________________________________________________________

A couple more examples to understand subqueries better:

  Select authors that have books within the price range of 500 to 700 and return the ``id``
  by descending order.

  .. code-block:: psql

    postgres=# SELECT first_name, last_name FROM author WHERE id IN (SELECT author_id FROM book WHERE price BETWEEN 500 AND 700 ORDER BY id DESC);
     first_name | last_name
    ------------+-----------
     John       | Doe
     Jane       | Doe
     Jane       | Foster
    (3 rows)


  A non-ideal example that selects ``author_id`` between 2 and 4, that returns the
  last names in descending order:

  .. code-block:: psql

    postgres=# SELECT first_name, last_name FROM author WHERE id BETWEEN (SELECT author_id FROM book WHERE author_id = 2) AND (SELECT author_id FROM book WHERE author_id = 4) ORDER BY last_name DESC;
     first_name | last_name
    ------------+-----------
     Jane       | Foster
     John       | Foster
     Jane       | Doe
    (3 rows)


8.2 Aliases
-----------
Aliases are temporary names given to a table or column. Alias renaming is a temporary
change until the query ends, it does not permanently alter the name of the table
or column.

Aliases are handy especially in ``JOINS`` clause since we're able to write the
query faster and have better readability.

For tables:

  syntax: ``SELECT column_name FROM table_name AS alias_name WHERE condition``

  example: ``SELECT a.first_name, b.title FROM author AS a, book AS b WHERE a.id = b.author_id;``

  .. code-block:: psql

    postgres=# SELECT a.first_name, b.title FROM author AS a, book AS b WHERE a.id = b.author_id;
     first_name |   title
    ------------+------------
     John       | Harry Pot
     John       | Potter Har
     Jane       | The Thing
     Jane       | The Car
     John       | The Wizard
    (5 rows)


  In the example above, we renamed ``author`` table into ``a`` and ``book`` to ``b``.

_______________________________________________________________________________

For columns:

  syntax: ``SELECT column_name AS alias_name FROM table_name WHERE condition``

  example: ``SELECT title AS name, price AS value FROM book;``

  .. code-block:: psql

    postgres=# SELECT title AS name, price AS value FROM book;
        name    | value
    ------------+-------
     Harry Pot  |   400
     Potter Har |   500
     The Thing  |   600
     The Car    |   700
     The Wizard |   800
    (5 rows)


  In the example above, we renamed ``title`` column into ``name`` and ``price`` to ``value``.

_______________________________________________________________________________

Let's apply aliases into subqueries. Suppose we want to find author names with books
priced 600 and above.

The inner query would look like this:

  ``SELECT author_id FROM book WHERE price >= 600;``

Let's use an alias for a column in the outer query:

  ``SELECT concat(first_name, ' ', last_name) AS full_name FROM author WHERE id IN(3, 4, 5);``

The ``concat`` command simply adds strings together. Now let's put them together and
add aliases for the tables.

  ``SELECT concat(a.first_name, ' ', a.last_name) AS full_name FROM author AS a
  WHERE id IN (SELECT b.author_id FROM book AS b WHERE b.price >= 600);``

  .. code-block:: psql

    postgres=# SELECT concat(a.first_name, ' ', a.last_name) AS full_name FROM author AS a
    WHERE id IN (SELECT b.author_id FROM book AS b WHERE b.price >= 600);
      full_name
    -------------
     Jane Doe
     Jane Foster
     John Foster
    (3 rows)

