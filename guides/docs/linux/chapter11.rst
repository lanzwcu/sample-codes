11 - Processes
==============
This chapter focuses on processes in Linux and how to view them.


11.1 What Are Processes
-----------------------
A process is a program that's been loaded from a long-term storage device into
your system's RAM and is being processed by the CPU.

Types of programs to create a process:

**Binary executables**
  Programs that runs through a compiler to create a binary executable file.

    example: programs created with C or C++

**Internal shell commands**
  Commands that are built in the shell program itself.

    example: shell commands like ``exit``

**Shell scripts**
  Text files that are executed through the shell.

    example: An executable text file that contains commands that needs to be run
    from the shell.

_______________________________________________________________________________

The Linux operating system can run multiple process concurrently at the same
time. This means that the CPU runs through multiple processes alternately, it
switches from one process to another.

The are 2 exceptions:

**Multicore CPU**
  This CPU has 2 cores that function as 2 separate CPUs, it runs processes
  two times a regular one.

**Multithreading/Hyperthreading CPU**
  One processor that can run 2 processes at the time, this type of processor
  can also multitask, which means it switches back and forth with another set
  of processes.

There is a more powerful CPU which is the multicore multithreading CPU.
Basically you have 2 sets of multithreading CPU. It works on 4 separate
processes concurrently at the same time.

_______________________________________________________________________________

Linux systems implements several types of processes. Some processes are:

**User processes**
  These processes are created by the user.

    example: applications that are run from the user's endpoint (browser)

  To view these processes type ``ps -a``

  .. code-block:: console

     vagrant@ubuntu-bionic:~/projects/documentation$ ps -a
       PID TTY          TIME CMD
      2504 pts/0    00:00:00 ps

**System processes**
  These processes are created by damons , most of the processes running in a
  Linux system are system processes.

  To view these processes type ``ps -e``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/projects/documentation$ ps -e
      PID TTY          TIME CMD
        1 ?        00:00:01 systemd
        2 ?        00:00:00 kthreadd
        3 ?        00:00:00 kworker/0:0
        4 ?        00:00:00 kworker/0:0H
        6 ?        00:00:00 mm_percpu_wq
        7 ?        00:00:00 ksoftirqd/0


11.2 What Are Processes Heredity
--------------------------------
All Linux processes came from 1 single process depending on which Linux
distribution. It could either be the ``init`` process or the ``systemd``
process. Both of these processes are started by the Linux kernel when the
system first boosts up.

Any processes are allowed to launch additional processes, they will have a
parent/child relationship. To identify which process is which, Linux assigns
2 unique resources for each process:

**Process ID (PID)**
  A unique ID for each and every process.

**Parent process ID (PPID)**
  This is the PID number of the parent of this process.

There is a grandparent process, this process is responsible for launching the
first processes. As discussed above it could either be ``init`` or ``systemd``.

.. note:: Kernel processes always has a PID of the value 0 and ``init`` /
          ``systemd`` will always have a PID of 1.


11.3 How To View Processes
--------------------------
.. _top:

**top**
  This command displays a table of some of the Linux processes that are running
  . Type in ``top`` to view the list:

  .. code-block:: console

    vagrant@ubuntu-bionic:~/projects/documentation$ top
    top - 04:42:13 up 19 min,  1 user,  load average: 0.07, 0.03, 0.04
    Tasks: 116 total,   1 running,  63 sleeping,   0 stopped,   0 zombie
    %Cpu(s):  1.0 us,  0.3 sy,  0.3 ni, 98.2 id,  0.2 wa,  0.0 hi,  0.1 si,  0.0 st
    KiB Mem :  2040860 total,  1473180 free,   100448 used,   467232 buff/cache
    KiB Swap:        0 total,        0 free,        0 used.  1777312 avail Mem

      PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
     2329 vagrant   20   0  107984   3348   2344 S   3.7  0.2   0:00.34 sshd
     2540 vagrant   20   0   42236   3752   3148 R   3.7  0.2   0:00.34 top
        1 root      20   0   77800   8936   6636 S   0.0  0.4   0:01.84 systemd
        2 root      20   0       0      0      0 S   0.0  0.0   0:00.00 kthreadd
        3 root      20   0       0      0      0 I   0.0  0.0   0:00.09 kworker/0:0
        4 root       0 -20       0      0      0 I   0.0  0.0   0:00.00 kworker/0:0H


  columns:

  - PR - priority (lower values have a higher priority)
  - NI - nice value (influences the PR)
  - VIRT - amount of virtual memory used
  - RES - amount of physical RAM using
  - SHR - amount if shared memory used
  - S - status of the process
      - D - uninterruptibly sleeping
      - R - running
      - S - sleeping
      - T - traced or stopped
      - Z - zombied
  - %CPU - percent of CPU time consumed by this process
  - %MEM - percent of available physical RAM being used by this process
  - TIME+ - total amount of CPU time consumed
  - COMMAND - name of the command that started the process

.. note:: Top changes constantly as the processes change. The downside of top
          is that it only shows a limited number of processes.

_______________________________________________________________________________

.. _ps:

**ps**
  The ``ps`` utility does not display a dynamic utility like ``top``. It only
  displays a snapshot of the current state of the processes. By default ``ps``
  only displays 4 columns and only shows the processes associated with the
  current shell session. Type in ``ps`` to view the snapshot:

  .. code-block:: console

   vagrant@ubuntu-bionic:~/projects/documentation$ ps
     PID TTY          TIME CMD
    2330 pts/0    00:00:00 bash
    2594 pts/0    00:00:00 ps
    2594 pts/0    00:00:00 ps


  columns:

  - PID - process ID
  - TTY - name of the shell session (? - system processes)
  - TIME - total amount of CPU time consumed
  - CMD -  name of the command that started the process

  To view more information, you must add options to this command, try
  ``ps -e``:

  .. code-block:: console

    vagrant@ubuntu-bionic:~/projects/documentation$ ps -e
      PID TTY          TIME CMD
        1 ?        00:00:01 systemd
        2 ?        00:00:00 kthreadd
        3 ?        00:00:00 kworker/0:0
        4 ?        00:00:00 kworker/0:0H
        6 ?        00:00:00 mm_percpu_wq
        7 ?        00:00:00 ksoftirqd/0


  To display even more information simply add -f option like so ``ps -ef``:

  .. code-block:: console

    vagrant@ubuntu-bionic:~/projects/documentation$ ps -ef
    UID        PID  PPID  C STIME TTY          TIME CMD
    root         1     0  0 04:23 ?        00:00:01 /sbin/init
    root         2     0  0 04:23 ?        00:00:00 [kthreadd]
    root         3     2  0 04:23 ?        00:00:00 [kworker/0:0]
    root         4     2  0 04:23 ?        00:00:00 [kworker/0:0H]
    root         6     2  0 04:23 ?        00:00:00 [mm_percpu_wq]
    root         7     2  0 04:23 ?        00:00:00 [ksoftirqd/0]


  additional columns:

  - UID - user ID
  - PPID - parent process ID
  - C - amount of processor time utilized by the process
  - STIME - time that the process started

  To step it up a bit further and display the long format add the -l option
  ``ps -elf``:

  .. code-block:: console

    vagrant@ubuntu-bionic:~/projects/documentation$ ps -elf
    F S UID        PID  PPID  C PRI  NI ADDR SZ WCHAN  STIME TTY          TIME CMD
    4 S root         1     0  0  80   0 - 19450 -      04:23 ?        00:00:01 /sbin/init
    1 S root         2     0  0  80   0 -     0 -      04:23 ?        00:00:00 [kthreadd]
    1 I root         3     2  0  80   0 -     0 -      04:23 ?        00:00:00 [kworker/0:0]
    1 I root         4     2  0  60 -20 -     0 -      04:23 ?        00:00:00 [kworker/0:0H]
    1 I root         6     2  0  60 -20 -     0 -      04:23 ?        00:00:00 [mm_percpu_wq]
    1 S root         7     2  0  80   0 -     0 -      04:23 ?        00:00:00 [ksoftirqd/0]

  additional columns:

  - F - flags
      - 1 - forked but didn't execute
      - 4 - used root privileges
  - S - status of the process the same with what we've seen on top_ command
  - PRI - the same with PR on top_
  - ADDR - memorry address of the process
  - SZ - size of the process
  - WCHAN - name of the kernel function which the process is sleeping

_______________________________________________________________________________

**free**
  The free command only displays 3 critical pieces of information (mem, swap
   and total). Type in ``free -mt``:

  .. code-block:: console

    vagrant@ubuntu-bionic:~/projects/documentation$ free -mt
                  total        used        free      shared  buff/cache   available
    Mem:           1993          98        1438          11         456        1735
    Swap:             0           0           0
    Total:         1993          98        1438

  The ``-m`` option displays the information in megabytes.

  The ``-t`` option displays the totals for each category of information.

_______________________________________________________________________________

**pgrep**
  Sometimes the ps_ command can be overwhelming, pgrep combines the
  functionality of ps and grep commands. Type in ``pgrep -l -u user_name``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/projects/documentation$ pgrep -l -u vagrant
    2208 systemd
    2209 (sd-pam)
    2329 sshd
    2330 bash

  options to specify what criteria:

  - P - ppid matches on the specified parent process ID
  - f - name matches on the specified process name
  - u - user_name matches on the specified process owner
  - l - displays the process name


