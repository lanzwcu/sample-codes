5 - File Ownership
==================
This chapter focuses on the file and directory ownership.


5.1 How Ownership Works
-----------------------
Ownership simply tells us who is allowed to do what with a file or directory.
The owner is granted with read, write and execute permissions for that file.
By default, whoever creates or copies a file is automatically set as the
owner of that file.

There are 2 types of ownership:

**user**
  The user who owns the file.
**group**
  The group of the user that owns the file.

To view the ownership of a file or directory, type ``ls -l`` in the command
line. This will display a table of files and its owners:

.. code-block:: console

  vagrant@ubuntu-bionic:~/projects/documentation$ ls -l
  total 58
  -rwxrwxrwx 1 vagrant vagrant  607 Sep  9 14:28 Makefile
  drwxrwxrwx 1 vagrant vagrant    0 Sep  9 14:28 _build
  drwxrwxrwx 1 vagrant vagrant    0 Sep  9 14:28 _static
  drwxrwxrwx 1 vagrant vagrant    0 Sep  9 14:28 _templates
  -rwxrwxrwx 1 vagrant vagrant 2387 Sep 16  2019 chapter01.rst

The 3rd column are the users and the 4th column are the groups.

Ownership is not fixed and can be modified. When using the commands to change
the user or group, you must either be logged in as root or have ``sudo`` access.

.. note:: You can also change the owning group if you are logged in as the
        user who owns the the file and is a member of the group to be
        assigned to.


5.2 How To Change Ownership
---------------------------
There are 2 commands that can be used to alter ownership:

**chown**
  This command stands for "change ownership" and is used to change the
  ownership of a file or directory.

    syntax: ``chown user.group_name file_or_dir``

    example ``chown mark document.txt`` or ``chown mark.users document.txt``

  To alter the group using ``chown``, simply put a ``.`` in front of
  the group name to specify that it is a group and not a user:

    syntax: ``chown .group_name file_or_dir``

    example: ``chown .users document.txt``

.. tip:: If you would like to change multiple files or directories that have
         subdirectories, you may add the ``-R`` option.

_______________________________________________________________________________

**chgrp**
  This command stands for "change group" and is used to change only the group
  ownership.

    syntax: ``chgrp group_name file_or_dir``

    example ``chown users document.txt``
