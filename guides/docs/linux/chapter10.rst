10 - File Archiving
===================
This chapter will focus on the ``tar`` command to create file archives. File
archives are used to collect multiple files and compress it into one file that
can be used as backup or to transfer files efficiently.


10.1 How To Create A New Archive File
-------------------------------------
**tar**
  This command stands for "tape archive" and is used when creating archive
  files.

    syntax: ``tar -cvf archive_filename directory``

    example: ``tar -cvf /media/usb/homebackup.tar /home``

  The ``c`` stands for create; ``v`` is optional, it tells tar to work in
  verbose mode; ``f`` is used to specify the name of the new archive to be
  created.

  The example will create an archive file of the /home directory.

.. note:: By default, tar will remove leading '/' from member names.


10.2 How To Extract Files From An Archive File
----------------------------------------------
The same command is used to extract files from the archive. Except that we will
use `x` which stands for extract, instead of `c`.

  syntax: ``tar -xvf archive_filename``

  example: ``tar -xvf /media/usb/homebackup.tar``


10.3 How To Compress An Archive File
------------------------------------
To compress an archive file we must change the options to:

  example: ``tar -cJvf /media/usb/homebackup.tar.xz /home``

  .. tip:: It is good practice to add the extension of the filename, in this
           case ``.xz``, to remind us what compression utility was used when
           the archive was created.