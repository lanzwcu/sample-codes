3 - Directories
===============
This chapter focuses on the basic commands used in Linux to create, view and
remove a directory.


3.1 What Is A Directory
------------------------
A directory contains files and or directories, its identifier is that is has a
forward slash(/) after the name. An example would be ``/projects/documentation/``.
This shows that the directory ``documentation/`` is under the directory
``projects/``


3.2 How To Create A Linux Directory
-----------------------------------
**mkdir**
  This command stands for "make directory" and is used to create a new
  directory.

    syntax: ``mkdir directory_name``

    example: ``mkdir new_directory``


3.3 How To View A Linux Directory
---------------------------------
**pwd**
  This command stands for "print working directory" and is used to show the
  current directory you are on.

    syntax: ``pwd``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/projects/documentation$ pwd
    /home/vagrant/projects/documentation

_______________________________________________________________________________

**ls**
  This command stands for "list" and is used to list the contents of a directory.
  If no directory is specified, ``ls`` displays the list of the current working
  directory.

  syntax: ``ls options file or directory``

  example ``ls new_directory``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/projects/documentation$ ls new_directory
    Excel.xlsx  sample.txt  test

  There are many options for ``ls`` like ``ls - l`` which displays a list with
  long format that shows the permissions:

  .. code-block:: console

    $ ls -l
    total 0
    -rw-r--r--  1 ryan  staff  0 Jan 22 12:43 file1
    -rw-r--r--  1 ryan  staff  0 Jan 22 12:43 file2
    -rw-r--r--  1 ryan  staff  0 Jan 22 12:43 file3


  To display hidden files and directories, i.e., those starting with ``.``, use
  the ``-a`` flag.:

  .. code-block:: console

    $ ls -l
    total 0
    -rw-r--r--  1 ryan  staff  0 Jan 22 12:43 file1
    -rw-r--r--  1 ryan  staff  0 Jan 22 12:43 file2
    -rw-r--r--  1 ryan  staff  0 Jan 22 12:43 file3

    $ ls -la
    total 0
    drwxr-xr-x   6 ryan  staff  192 Jan 22 12:46 .
    drwxr-xr-x+ 29 ryan  staff  928 Jan 22 12:43 ..
    -rw-r--r--   1 ryan  staff    0 Jan 22 12:46 .iamhidden
    -rw-r--r--   1 ryan  staff    0 Jan 22 12:43 file1
    -rw-r--r--   1 ryan  staff    0 Jan 22 12:43 file2
    -rw-r--r--   1 ryan  staff    0 Jan 22 12:43 file3


  Browse through the manual to learn more.


3.4 How To Navigate Through A Linux Directory
---------------------------------------------
**cd**
  This command stands for "change directory" and is used to navigate through
  the directories.

    syntax: ``cd directory_name``

    example: ``cd new_directory``

  You can also immediately return to the home directory by putting in ``~`` as
  the directory_name like this ``cd ~``:

  .. code-block:: console

    vagrant@ubuntu-bionic:~/projects/documentation$ cd ~
    vagrant@ubuntu-bionic:~$ cd projects


  You can also traverse directories by utilizing ``..``.

  To go up by 1 directory, use ``cd ..``:

  .. code-block:: console

    $ pwd
    /Users/ryan/test/dir1/dir2

    $ cd ..

    $ pwd
    /Users/ryan/test/dir1


  You can repeat this multiple times to go back up by X times. For example,
  to go back up by 2 directories, use ``cd ../..``. To go back up 3 directories,
  use ``cd ../../..``, etc.:

  .. code-block:: console

    $ pwd
    /Users/ryan/test/dir1/dir2

    $ cd ../..

    $ pwd
    /Users/ryan/test


3.5 How To Remove A Linux Directory
-----------------------------------
**rmdir**
  This command stands for "remove directory" and is used to remove directories,
  this command will fail when used on a file. This command only removes empty
  directories

    syntax: ``rmdir options directory_name``

    example: ``rmdir new_directory/test/``

  ``test/`` in this case is an empty directory. If you want to delete a
  directory with files, you must use the ``rm`` command with the recursive
  option ``-r``:

    example: ``rm -r new_directory``

.. note:: If you remove a directory using ``rmdir``, the directory cannot be
          recovered. The same goes with the ``rm -r`` command.