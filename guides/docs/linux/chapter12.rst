12 - Process Prioritization And Termination
===========================================
This chapter focuses on how to prioritize process on a Linux system. By default
, Linux tries to equalize the amount of CPU time given to all processes running.

Keep in mind that the lower the value of priority is, the higher its priority
is. A low value of nice means that the process is less nice, which demands more
CPU time.

.. note:: The nice value can range from -20 to +19.


12.1 How To Prioritize A Process
--------------------------------
You cannot directly manipulate the priority of a process, to indirectly
manipulate it we must change the nice value.

**nice**
  The nice is used to change the nice level before the process runs.

    syntax: ``nice -n nice_level command``

    example: ``nice -n -15 gedit``

_______________________________________________________________________________

**renice**
  This command is used to modify the nice value when the process is already
  running.

    syntax: ``renice -n nice_level PID``

    example: ``renice -n 5 4321``


.. note:: Linux won't allow you to drop the nice value below 0 unless you are
          logged in as root or have elevated access.


12.2 How To Terminate A Process
-------------------------------
Normally processes have an exit function, but in the event that a process hangs
, we will need to use either of these commands:

.. _kill:

**kill**
  This command is used to terminate a running process. It has several types
  of kill signals, here are some:

  - \(1) SIGHUP - lightest way to end a process
  - \(2) SIGINT - acts like CTRL + C
  - \(9) SIGKILL - does not allow the process to clean up (most brutal)
  - \(15) SIGTERM - default if no signal is specified

  You can either use the either the name or the number of the kill signal.

    syntax: ``kill -signal PID``

    example: ``kill -SIGNTERM 4321`` or ``kill -15 4321``

_______________________________________________________________________________

**killall**
  This command is very similar to kill_ . The main difference is that with
  killall, you specify the process to be killed instead of the process ID (PID).

    syntax: ``killall -signal command``

    example: ``killall -15  gedit``

_______________________________________________________________________________

**pkill**
  This command's syntax is similar with pgrep command. The same concept applies
  wherein you can use pkill to match a specific search criteria and terminate
  those matches.

    example: ``pkill -SIGTERM -f gedit``


.. tip:: It is advisable to try the less brutal way of kill signals first.


12.3 How To Keep A Process Running Even If You Logged Out
---------------------------------------------------------
Linux automatically sends a SIGHUP signal to all programs associated to the
session when the user logs out. Normally each process will hung up, however a
process can be told to ignore this signal using:

**nohup**
  This command stands for "no hang-up" and is used to ignore the SIGHUP signal
  only.

    example: ``nohup command_name &``

  The ``&`` at the end tells Linux to run this process in the background.

  .. note:: If the process generates a file or error in the process, it will be
            stored under ~ /nohup.out


