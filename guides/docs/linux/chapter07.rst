7 - File Permissions
====================
File ownership focuses on who owns a file or directory only, it does not handle
any permissions for a file. This chapter covers the types permissions and how to
set them.

The permission type of a file or directory can be found in the 1st column when
you type ``ls -l`` on the command line:

.. code-block:: console

  vagrant@ubuntu-bionic:~/projects/documentation$ ls -l
  total 58
  -rwxrwxrwx 1 vagrant vagrant  607 Sep  9 14:28 Makefile
  drwxrwxrwx 1 vagrant vagrant    0 Sep  9 14:28 _build
  drwxrwxrwx 1 vagrant vagrant    0 Sep  9 14:28 _static
  drwxrwxrwx 1 vagrant vagrant    0 Sep  9 14:28 _templates
  -rwxrwxrwx 1 vagrant vagrant 2387 Sep 16  2019 chapter01.rst

The breakdown of this mode is discussed in 7.3_

7.1 How Permissions Work
------------------------
Permissions basically determine what a user can or cannot do.

There are 3 types of Linux permissions:

+------------+--------+---------------------------+---------------------------+
| Permission | Symbol | File                      | Directory                 |
+============+========+===========================+===========================+
| Read       | r      | Allows user to open/view a| Allows user to list the   |
|            |        | file.                     | contents of a directory.  |
+------------+--------+---------------------------+---------------------------+
| Write      | w      | Allows user to edit and   | Allows user to add/remove |
|            |        | save changes.             | files from the directory  |
+------------+--------+---------------------------+---------------------------+
| Execute    | x      | Allows user to run an     | Allows user to enter a    |
|            |        | executable file.          | directory                 |
+------------+--------+---------------------------+---------------------------+

.. _above:

These permissions can also be represented numerically:

+------------+-------+
| Permission | Value |
+============+=======+
| Read       | 4     |
+------------+-------+
| Write      | 2     |
+------------+-------+
| Execute    | 1     |
+------------+-------+


7.2 File System Entities
------------------------
The 3 types of permissions listed above can be assigned to different entities
for each and every file or directory.

3 types of file system entities:
  **owner**
    The owner of the file or directory. By default the owner is granted with
    read/write access for files and read/write/execute for directories.
  **group**
    Members of the group will inherit whatever permissions are applied to the
    group.
  **others**
    Refers to all other authenticated users on the Linux system, who is neither
    the owner nor a member of the owning group.

.. note:: It is best practice to only assign minimal permissions to others.


.. _7.3:

7.3 How To Read A Permission Mode
---------------------------------

Now let's take a look at a 10-character permission mode:

    **- r w x r - x - - -**

It can be broken down into 4 parts:

- (-) 1st character determines whether it is a file, dir or a symbolic link.
    - d : directory
    - \- : file
    - l : symbolic link
- (r w x) 2nd 3rd 4th characters determine the permissions for the owner.
- (r - x) 5th 6th 7th characters determine the permissions for the group.
- (- - -) 8th 9th 10th characters determine the permissions for others.

_______________________________________________________________________________

The numerical equivalent of the example permission above is *750*, you
can refer to the table above_ for the corresponding permission of the number.
You will have to add those numbers up to determine the allowed permissions.
This can be broken down into 3 parts:

- \(7) 1st digit determines the permissions for the owner. (4 + 2 + 1)
- \(5) 2nd digit determines the permissions for the group. (4 + 1)
- \(0) 3rd digit determines the permissions for the owners. (0)

_______________________________________________________________________________

This means that for the example above

- Is responsible for the permission of a file.
- The owner can perform read, write or execute.
- The members of the owning group can read and execute.
- Others are not allowed to read, write or execute.


7.4 How To Alter Permissions
----------------------------

**chmod**
  This command stands for "change mode" and is used when modifying permissions.
  There are different syntax for this command:

  - The first syntax overrides what is already in the mode for the user.

      syntax 1: ``chmod entity=permissions filename``

      example 1: ``chmod u=rwx,g=rw,o=r document.txt``

      .. note:: ``u`` in this command is the owner; ``g`` is the group; ``o``
              refers to others.

  - The second syntax uses ``+`` and ``-`` signs to add or remove a specific
    permission.

      syntax 2: ``chmod entity+/-permission filename``

      example 2 (remove): ``chmod g-w document.txt``

      example 2 (add): ``chmod g+w document.txt``

  - The third syntax represents the entire mode to be assigned using numerical
    permissions.

      syntax 3: ``chmod numerical_permission filename``

      example 3: ``chmod 640 document.txt``

.. tip:: If you would like to change multiple files or directories that have
          subdirectories, you may add the ``-R`` option.