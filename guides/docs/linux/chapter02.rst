2 - Files
=========
This chapter focuses on the basic commands used in Linux to create, view and
remove a file.


2.1 What Is A File
------------------
A File is a computer object that can store a variety of information, it can be
of different types as well such as .txt, .log, .rst, .exe, etc.


2.2 How To Create A Linux File
------------------------------
There are several ways to create a file from a Linux terminal, below are some:

**touch**
  This command is the standard way to create empty file/s, you can specify
  which file type you want to be created by addition the desired extension.

    syntax: ``touch file_name``

    example: ``touch new_text_file.txt``

  To create multiple files using 1 command just separate the file names with
  spaces.

    example: ``touch new_text_file.txt another_text.txt another_one.txt``

_______________________________________________________________________________

**cat**
  This command stands for "concatenate" and is often used to view files, it can
  also be used to create files with content.

    syntax: ``cat file_name``

    example: ``cat new_file.txt``

  After running the command above, the terminal will be in a state wherein you
  can type in whatever you need which will used as the content of the file.
  Use ``CTRL + C`` to tell Linux you are done with the content.


.. note:: ``touch`` and ``cat`` also offer other functions besides creating,
          see ``man`` of these commands to learn more.


2.3 How To View A Linux File
----------------------------
To view a file we can use either of the following commands:

**cat**

  syntax: ``cat file_name``

  example: ``cat new_file.txt``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/projects/documentation$ cat new_file.txt
    this
    is
    a
    new
    file
    created


You can opt to display the output in reverse order by changing ``cat`` to ``tac``

_______________________________________________________________________________

**head**
  This command shows the initial lines of a file. By default, it shows the
  first 10 lines of the specified file.

    syntax: ``head option file_name``

    example: ``head -n 3 new_file.txt``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/projects/documentation$ head -n 3 new_file.txt
    this
    is
    a


  ``-n 3`` tells Linux that we only want the first 3 lines of the file to be
  displayed.

_______________________________________________________________________________

**tail**
  This command is the counter part of the ``head`` command. Which means it
  shows the last lines of the file. By default, it shows the last 10 lines.

    syntax: ``tail option file_name``

    example: ``tail -n 3 new_file.txt``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/projects/documentation$ tail -n 3 new_file.txt
    new
    file
    created


2.4 How To Remove A Linux File
------------------------------
**rm**
  This command stands for "remove" and is used to remove files, not directories.

    syntax: ``rm options file_name``

    example: ``rm new_file.txt``

  It works the same with ``touch`` when deleting multiple files you can do:

    example: ``rm new_text_file.txt another_text.txt another_one.txt``

  You can use the ``-i`` option to have a confirmation before deletion:

    example: ``rm -i new_file.txt``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/projects/documentation$ rm -i new_file.txt
    rm: remove regular file 'new_file.txt'? n


  Type in ``y`` if you would like to proceed with the deletion, otherwise type
  ``n``.

  If you do not want to be prompted with a confirmation before the deletion of 
  a file, i.e., you are absolutely sure you want to delete a file, use the 
  ``-f`` flag.

  .. code-block:: console

    $ ll
    total 0
    drwxr-xr-x   3 ryan  staff   96 Jan 22 11:48 .
    drwxr-xr-x+ 28 ryan  staff  896 Jan 22 11:47 ..
    -rw-r--r--   1 ryan  staff    0 Jan 22 11:48 donotdelete.me
    
    $ rm -f donotdelete.me 
    $ ll
    total 0
    drwxr-xr-x   2 ryan  staff   64 Jan 22 11:48 .
    drwxr-xr-x+ 28 ryan  staff  896 Jan 22 11:47 ..


  .. note:: If a file does not exist and you attempt to use the ``-f`` flag with
    ``rm``, it will not display any warning message that the file you deleted
    does not exist.

  .. code-block:: console

    $ ll
    total 0
    drwxr-xr-x   2 ryan  staff   64 Jan 22 11:48 .
    drwxr-xr-x+ 28 ryan  staff  896 Jan 22 11:47 ..
    $ rm -f donotdelete.me
    $ ll
    total 0
    drwxr-xr-x   2 ryan  staff   64 Jan 22 11:48 .
    drwxr-xr-x+ 28 ryan  staff  896 Jan 22 11:47 ..


