9 - SPECIAL PERMISSIONS
=======================
On the previous chapters, we've discussed the common permissions. This chapter
will focus on special permissions.


9.1 What Are Special Permissions
--------------------------------
These special permissions are designed for a specific purpose. There are 3
types of special permissions:

+------------+---------------------------+------------------------------------+
| Permission | File                      | Directory                          |
+============+===========================+====================================+
| SUID (set  | Owner who runs the file,  | None                               |
| user ID)   | temporarily becomes the   |                                    |
|            | file's owner.             |                                    |
+------------+---------------------------+------------------------------------+
| SGID (set  | User who runs the file,   | When a user creates a file in the  |
| group ID)  | temporarily becomes a     | directory, the owning group is set |
|            | member of the file's      | to the owning group of the parent  |
|            | owning group.             | directory.                         |
+------------+---------------------------+------------------------------------+
| Sticky Bit | None                      | Users can only delete files within |
|            |                           | the directory for which they are   |
|            |                           | the owner of the file or the       |
|            |                           | directory itself.                  |
+------------+---------------------------+------------------------------------+

Just like the regular permissions, these permissions are assigned to a
numeric value:

+------------+-------+
| Permission | Value |
+============+=======+
| SUID       | 4     |
+------------+-------+
| SGID       | 2     |
+------------+-------+
| Sticky Bit | 1     |
+------------+-------+

_______________________________________________________________________________


9.2 How To Assign Special Permissions
-------------------------------------
**chmod**
    syntax: ``chmod numerical_permission filename``

    example: ``chmod 7750 document.txt``

The example uses all special permissions.

- \(7) 1st digit determines the special permissions. (4 + 2 + 1)
- \(7) 2nd digit determines the permissions for the owner. (4 + 2 + 1) (r w x)
- \(5) 3rd digit determines the permissions for the group. (4 + 1) (r - x)
- \(0) 4th digit determines the permissions for the owners. (0) (- - -)

The permission of the file will be changed from this:
  **r w - r w - r w -**

to this:
  **r w s r - s - - T**

Notice that instead of **x** or **-**, we are dealing with **s** and **T**. If
there is an **s** in the permission for the owner or group, this means that in
this case SUID and SGID are both active. **T** on the other's permission tells
us that the sticky bit has been set.


