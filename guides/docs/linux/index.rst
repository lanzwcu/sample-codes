.. Linux Guide documentation master file, created by
   sphinx-quickstart on Mon Sep  9 12:16:12 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Linux Basic Commands
====================

.. image:: /images/linux_banner.png

This documentation covers some of the frequently used Linux commands. Linux is
an open source operating system.


.. toctree::
   :maxdepth: 2

   chapter01
   chapter02
   chapter03
   chapter04
   chapter05
   chapter06
   chapter07
   chapter08
   chapter09
   chapter10
   chapter11
   chapter12

