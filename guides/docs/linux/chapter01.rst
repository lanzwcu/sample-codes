1 - Linux Manual Command
========================

This chapter focuses on a very handy command called ``man``. It provides the
name, syntax, description and a list of options if there are any for the given
command.

**man**
  This command stands for "manual" and is used to get help on any Linux command.

    syntax: ``man command_name``

    example: ``man ls``

  The example above will display the manual for ``ls``:

  .. code-block:: console

    LS(1)                                                  User Commands                                                 LS(1)

    NAME
           ls - list directory contents

    SYNOPSIS
           ls [OPTION]... [FILE]...

    DESCRIPTION
           List information about the FILEs (the current directory by default).  Sort entries alphabetically if none of -cftu‐
           vSUX nor --sort is specified.

           Mandatory arguments to long options are mandatory for short options too.

           -a, --all
                  do not ignore entries starting with .

           -A, --almost-all
                  do not list implied . and ..

           --author
                  with -l, print the author of each file

           -b, --escape
    Manual page ls(1) line 1 (press h for help or q to quit)


  Use your keyboard's navigation keys to scroll up or down the manual. You can
  press the letter ``h`` key for help and press the letter ``q`` to close the
  manual.

.. note:: If you would like to know more about ``man`` command, you may use
          the command to itself like so: ``man man``.