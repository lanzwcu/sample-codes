4 - Commands For Files And Directories
======================================

4.1 How To Search Through A File Or Directory
---------------------------------------------
For whatever reason **grep** is not installed in your machine, simply install
using the following commands:

Debian or Ubuntu: ``sudo apt-get install grep``

Rhel/CentOS/Fedora ``sudo yum install grep``

**grep**
  This command stands for "global regular expressions print" and is used to
  search a particular string or pattern in a file.

    syntax: ``grep options file_name``

  If no file is specified, then ``grep`` by default will search the current
  working directory.

  Let's create a simple text file named ``new_file.txt`` and input this
  sentence "This text will be used as an example for GrEp."

  This example displays the line where the string was found:

    example 1: ``grep example new_file.txt``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/projects/documentation$ grep example new_file.txt
    This text will be used as an example for GrEp

_______________________________________________________________________________

  Keep in mind that ``grep`` searches in a case-sensitive manner, which means
  if we try to search the file using the string "grep" instead of "GrEp", it
  will not return any output. If you want to ignore its case-sensitive nature,
  add the ``-i`` option like so:

    example 2: ``grep -i grep new_file.txt``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/projects/documentation$ grep grep new_file.txt
    vagrant@ubuntu-bionic:~/projects/documentation$ grep GrEp new_file.txt
    This text will be used as an example for GrEp
    vagrant@ubuntu-bionic:~/projects/documentation$ grep -i grep new_file.txt
    This text will be used as an example for GrEp


_______________________________________________________________________________

  To count the number of lines that a certain string appears in a file we use
  the ``-c`` option. Let's try to look for the letter "a":

    example 3: ``grep -c a new_file.txt``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/projects/documentation$ grep -c a new_file.txt
    1


_______________________________________________________________________________

  To count how many times a string appears in a file we use the ``-o`` option,
  accompanied by ``wc`` command to count the result for us.

    example 4: ``grep -o a new_file.txt | wc -l``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/projects/documentation$ grep -o a new_file.txt | wc -l
    3


  .. note:: piping or using ``|`` at the end of one command passes the output of
    one command to the next. Using the example above, ``grep -o a new_file.txt`` 
    outputs:

  .. code-block:: console

    $ grep -o a new_file.txt 
    a
    a
    a


  Those 3 lines will then be given to ``wc`` as its input argument.

_______________________________________________________________________________

  Lastly we can use regular expressions as a lookup key, let's try to lookup
  lineS that starts with "this":

    example 5: ``grep -i '^this' new_file.txt``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/projects/documentation$ grep -i '^this' new_file.txt
    This text will be used as an example for GrEp


_______________________________________________________________________________

  A note on using quotes. Quotes are primarily used to deal with whitespaces. 

  In the example below, a new line "hello world!" has been added at the end.

  .. code-block:: console

    $ grep hello world new_file.txt 
    grep: world: No such file or directory
    new_file.txt:hello world!


  ``grep`` looks for the string "hello" on files ``world`` and 
  ``new_file.txt``. But since the file ``world`` does not exist, it returns the
  ``No such file or directory`` error message.

  .. code-block:: console

    $ VAR="hello"

    $ grep '$VAR' new_file.txt


  Using single quotes ``''`` will look for the literal value of the string
  inside the quotes, in this example, that would be "$VAR".

  .. code-block:: console

    $ VAR="hello"

    $ grep "$VAR" new_file.txt
    hello world!


  Using double quotes ``""`` will look for the converted value of $VAR, which,
  in this case, is "hello".

  Lastly, know when to use backquotes or backticks ``"`"`` as these are used to
  run commands specified inside. 

  .. code-block:: console

    $ grep `echo hello` new_file.txt 
    hello world!

    $ whoami
    ryan
    $ grep `whoami` new_file.txt
    $


4.2 How To Copy A File Or Directory
-----------------------------------
.. _cp:

**cp**
  This command stands for "copy" and is used for copying files and directories.
  If the destination file does not exist, ``cp`` creates the file for you.

    syntax: ``cp options source destination``

    example: ``cp new_file.txt copied_file.txt``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/projects/documentation/new_dir$ ls
    new_file.txt  test.txt
    vagrant@ubuntu-bionic:~/projects/documentation/new_dir$ cp new_file.txt copied_file.txt
    vagrant@ubuntu-bionic:~/projects/documentation/new_dir$ ls
    copied_file.txt  new_file.txt  test.txt


  By default, ``cp`` does not copy directories. The workaround for this is to
  add the recursive ``-R`` option:

    example: ``cp -R new_dir/ copied_dir``


_______________________________________________________________________________

**cat**
  This was already tackled in the previous chapters. It can also be used to
  copy the contents of one file to another:

    syntax: ``cat source > destination``

    example ``cat new_file.txt > copied_file.txt``

  .. note:: using ``>`` is called output redirection. As the name implies, the
    output of the leftmost command is re-directed to the second one instead of,
    normally, to the console. In the example above, since the second part is a 
    file, the contents of the output will be dumped there. Any existing content
    will be overwritten. 
    
    If overwriting existing content is to be avoided, use ``>>`` instead. This 
    will only append the received output to the end of the file. 


4.3 How To Move Or Rename A File Or Directory
---------------------------------------------
**mv**
  This command stands for "move" and is used to move one or more files or
  directories. It can also rename a file or folder.

    syntax: ``mv options source destination``

    example: ``mv new_file.txt copied_file.txt``

  The key difference of ``mv`` from ``cp`` is that ``mv`` removes the source
  file. Notice that the ``new_file.txt`` is replaced by ``moved_file.txt``, it
  basically changed the name.

  .. code-block:: console

    vagrant@ubuntu-bionic:~/projects/documentation/new_dir$ ls
    new_file.txt  test.txt
    vagrant@ubuntu-bionic:~/projects/documentation/new_dir$ mv new_file.txt moved_file.txt
    vagrant@ubuntu-bionic:~/projects/documentation/new_dir$ ls
    moved_file.txt  test.txt

  ``mv`` by default is recursive unlike cp_, we do not need to specify the
  ``-R`` option

  .. code-block:: console

    vagrant@ubuntu-bionic:~/projects/documentation/sample$ ls
    new_dir  new_text.txt
    vagrant@ubuntu-bionic:~/projects/documentation/sample$ cd new_dir
    vagrant@ubuntu-bionic:~/projects/documentation/sample/new_dir$ ls
    nested_dir  sample  test.txt
    vagrant@ubuntu-bionic:~/projects/documentation/sample/new_dir$ cd ..
    vagrant@ubuntu-bionic:~/projects/documentation/sample$ mv new_dir moved_dir
    vagrant@ubuntu-bionic:~/projects/documentation/sample$ ls
    moved_dir  new_text.txt
    vagrant@ubuntu-bionic:~/projects/documentation/sample$ cd moved_dir
    vagrant@ubuntu-bionic:~/projects/documentation/sample/moved_dir$ ls
    nested_dir  sample  test.txt

  In the example above, the directory name ``new_dir`` has been changed to
  ``moved_dir`` and retains the same contents.