8 - Umask
=========
This chapter focuses on how to work with default Linux permissions. By default,
Linux assigns the permissions as:

+-------------+-------------------+---------+
|             | permission        | numeric |
+=============+===================+=========+
| files       | r w - r w - r w - | 666     |
+-------------+-------------------+---------+
| directories | r w x r w x r w x | 777     |
+-------------+-------------------+---------+

To increase security, the default permissions can be changed using ``umask``.

**umask**
  Is a 3-digit number that deducts permissions from the default provided by
  Linux. The default value of umask is either 0002 or 0022. The last 3 digits
  are permissions to be deducted from the default provided by Linux. They
  follow the same pattern of numerical permissions.

    syntax: ``umask numerical_permission``

    example: ``umask 023``

After applying the new umask:

- \(0) No permission will be deducted from the owner's permission.
- \(2) write permission will be deducted from owning group's permission.
- \(3) write and execute will be deducted from other's permissions.

  +-------------+--------------------+-------+----------------------+
  |             | permission         | umask | effective permission |
  +=============+====================+=======+======================+
  | files       | r w - r w - r w -  | 023   | r w - r - -r - -     |
  +-------------+--------------------+-------+----------------------+
  | directories | r w x r w x r w x  | 023   | r w x r - x r - -    |
  +-------------+--------------------+-------+----------------------+


.. note:: Be aware that umask restores its original value when the system is
          restarted. The workaround for this is to edit the appropriate
          configuration file and set the umask. It can be found in either
          ``/etc/profile`` or ``/etc/login.defs``, depending which distribution
          you are using.
