6 - Group Management
====================
This is chapter focuses on how to manage Linux groups from the shell.

Just like managing ownership, you must either be logged in as root or have
elevated access to add, modify or delete groups. You can view the existing
groups using the command ``tail /etc/group``, it will display something like
this:

.. code-block:: console

  vagrant@ubuntu-bionic:~/projects/documentation$ tail /etc/group
  landscape:x:112:
  admin:x:113:
  netdev:x:114:ubuntu
  vboxsf:x:115:
  vagrant:x:1000:
  ubuntu:x:1001:
  ssl-cert:x:116:postgres
  postgres:x:117:
  new_group:x:1002:


6.1 How To Add A Group
----------------------
**groupadd**
  This command stands for "group add" and is used to create a new Linux group.

    syntax: ``groupadd options group_name``

    example: ``groupadd new_group``


6.2 How To Modify A Group To Add A User
---------------------------------------
There are 2 commands that can be used to add a user to a group:

**usermod**
  This command stands for "user modify" and is used to add a group into an
  individual user.

    syntax: ``usermod options group_name user``

    example: ``usermod -g new_group new_user``


.. tip:: The lowercase ``-g`` changes the group of the user. But be careful when
         using the uppercase ``-G`` option, it will replace all of the groups
         of the owner with what is provided in the command. To avoid this, you
         have to add ``-a``, this will only append the group instead of
         replacing the existing groups.

_______________________________________________________________________________

**groupmod**
  This command stands for "group modify" and is used to edit the group itself
  to add users as members.

    syntax: ``groupmod options group_name``

    example 1 (add): ``groupmod -A "new_user" new_group``

    example 2 (remove): ``groupmod -R "new_user" new_group``


.. warning:: groupmod is not well supported and is only available on some Linux
             distributions.

6.3 How To Delete A Group
-------------------------
**groupdel**
  This command stands for "group delete" and is used to delete the group.

    syntax: ``groupdel group_name``

    example: ``groupdel new_group``
