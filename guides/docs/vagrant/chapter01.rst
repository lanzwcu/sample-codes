***********
1 - Vagrant
***********

Vagrant is a tool for building and managing virtual machine environments in a 
single workflow. With a focus on automation, Vagrant lowers environment setup
time, increases production, and avoids the "works on my machine" scenario.


====================
1.1 Why use Vagrant?
====================

Vagrant uses configurable, reproducible, and portable work environments built
on top of industry-standard technologies and controlled by and single, 
consistent workflow. Vagrant ships out-of-the-box with support for VirtualBox,
Hyper-V, and Docker, but also has the ability to manage other types of machines
as well. 

Vagrant is designed for everyone as the easiest and fastest way to create a 
virtualized environment.