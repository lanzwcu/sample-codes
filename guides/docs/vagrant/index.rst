.. Vagrant Guide documentation master file, created by
   sphinx-quickstart on Wed Jan 29 14:38:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Vagrant Cheatsheet
==================

.. image:: /images/vagrant_banner.png

This documentation covers why Vagrant should be used and how to go about and
set up an installation of your own.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   chapter01
   chapter02
   chapter03
   chapter04

