************************
3 - Vagrantfile and more
************************

This chapter will discuss what a Vagrantfile is and other useful concepts 
used in Vagrant. 


===============
3.1 Vagrantfile
===============

This is a file - with the actual literal name of Vagrantfile - used to describe 
the type of machine required for a project. This also describes how the machines
are configured and provisioned. Vagrantfile's syntax is *primarily* in Ruby_, 
but since it is mostly simple variable assignment, no prior knowledge in 
the Ruby programming language is necessary.

.. _Ruby: http://www.ruby-lang.org/

Vagrantfile has a twofold purpose:

	1.	Mark the root directory of your project. Many of the configuration 
	options in Vagrant are relative to this root directory, and

	2.	Describe the kind of machine and resources you need to run your project,
	as well as what software to install and how you want to access it.

Vagrant has a built-in command to initialize a directory to be used with 
Vagrant: ``vagrant init``. This will place a Vagrantfile in your current 
directory. It can also be run on a pre-existing directory to set up Vagrant for 
an existing project. 

.. tip:: 

	By default, once inside the VM (virtual machine), the directory where the 
	Vagrantfile is 	stored can be accessed by going to the **/vagrant** 
	directory.

	.. code-block:: console

		vagrant@vagrant:~$ ll /vagrant/Vagrantfile 
		-rw-r--r-- 1 vagrant vagrant 3024 Jan 29 10:28 /vagrant/Vagrantfile

=========
3.2 Boxes
=========

Boxes are base images that are used to quickly clone a virtual machine. Instead 
of building a VM from scratch - which is a slow and tedious 
process - these boxes are used instead. 

Specifying the box to be used in your Vagrant environment is the first step 
after creating a new Vagrantfile.

Running ``vagrant init`` will already install a box. But going into a bit more 
detail on how boxes are managed, ``vagrant box add`` will store the box under a 
specific name so multiple Vagrant environments can re-use it. 

.. code-block:: console

	$ vagrant box add hashicorp/bionic64
	==> box: Loading metadata for box 'hashicorp/bionic64'
	    box: URL: https://vagrantcloud.com/hashicorp/bionic64
	This box can work with multiple providers! The providers that it
	can work with are listed below. Please review the list and choose
	the provider you will be working with.

	1) hyperv
	2) virtualbox
	3) vmware_desktop

	Enter your choice: 2
	==> box: Adding box 'hashicorp/bionic64' (v1.0.282) for provider: virtualbox
	    box: Downloading: https://vagrantcloud.com/hashicorp/boxes/bionic64/versions/1.0.282/providers/virtualbox.box
	    box: Download redirected to host: vagrantcloud-files-production.s3.amazonaws.com
	==> box: Successfully added box 'hashicorp/bionic64' (v1.0.282) for 'virtualbox'!

	$ vagrant box list
	hashicorp/bionic64 (virtualbox, 1.0.282)


.. note:: 

	Option 2 (virtualbox) was chosen since this is the installed provider 
	on the author's local machine. 

	Boxes are namespaced -- this can be broken down into 2 parts, the username 
	and the box name, separated by a slash. In the command above, the username 
	is **hashicorp**, while the box name is **bionic64**. 


Running the command will download a box named **hashicorp/bionic64** from 
`HashiCorp's Vagrant Cloud box catalog`_.

.. _`HashiCorp's Vagrant Cloud box catalog`: https://vagrantcloud.com/boxes/search


.. tip:: 

	HashiCorp's Vagrant Cloud box catalog is a place to find and host 
	boxes. While convenient to download boxes, boxes can also be called from a 
	local file, custom URL, etc. 


Each project uses a box as an initial image to clone from and never modifies the
actual base image. This means that if there are two projects both using the 
**hashicorp/bionic64**, modifying files in one guest machine will *not* have an 
effect on the other machine. 


-----------------
3.2.1 Using a box
-----------------

Now that a box has been added, this will be configured to be used by the project
as a base. Edit the Vagrantfile and change the contents to the following:

.. code-block:: ruby

	Vagrant.configure("2") do |config|
	  config.vm.box = "hashicorp/bionic64"
	end


The **hashicorp/bionic64** should match the name used to add the box previously. This is how Vagrant knows which box to use. 

.. note:: 

	If the box was not added before, Vagrant will automatically download and add
	the box when it is run.


.. tip::

	You may add other variables to specify a specific config for your 
	environment. A few examples below.

		**config.vm.box_version**

			Specify an explicit version of a box.

			.. code-block:: ruby

				Vagrant.configure("2") do |config|
				  config.vm.box = "hashicorp/bionic64"
				  config.vm.box_version = "1.1.0"
				end


		**config.vm.box_url**

			Specify the URL to a box directly.

			.. code-block:: ruby

				Vagrant.configure("2") do |config|
				  config.vm.box = "hashicorp/bionic64"
				  config.vm.box_url = "https://vagrantcloud.com/hashicorp/bionic64"
				end


================
3.3 Provisioning
================

A VM running with the same OS (operating system) used/tested by multiple users 
is a good thing for consistency among other things, but it is rarely used just 
this way. Software packages are always installed on top of the OS so it can do 
other functions, such as host a website, a mail server, an NTP server, etc.

To ensure the same software  - and software version - is running right from the 
very beginning on anyone using the Vagrant environment, a few things need to be 
done. 

In the example below, **Apache**, a web server, will be provisioned. 

.. note::

	Since multiple files and folders will need to be added, the assumption will 
	be made that these will be placed in a repository, like Gitlab_, thus 
	managing new or updated files and folders will be dealt with separately. 

	.. _Gitlab: https://gitlab.com/


-------------------------
3.3.1 Create bootstrap.sh
-------------------------

Create the following shell script and save it as **bootstrap.sh** in the same 
directory as the Vagrantfile.

.. code-block:: bash

	#!/usr/bin/env bash

	apt-get update
	apt-get install -y apache2
	if ! [ -L /var/www ]; then
	  rm -rf /var/www
	  ln -fs /vagrant /var/www
	fi


-----------------------
3.3.2 Configure Vagrant
-----------------------

Add the ``provision`` line in the Vagrantfile.

.. code-block:: ruby

	Vagrant.configure("2") do |config|
	  config.vm.box = "hashicorp/bionic64"
	  config.vm.provision :shell, path: "bootstrap.sh"
	end


The ``provision`` line tells Vagrant to use the **shell** provisioner to setup 
the machine, with the **bootstrap.sh** file. The file path is relative to the 
location of the project root (i.e., where the Vagrantfile is).


----------------------
3.3.3 Add HTML content
----------------------

Finally, some HTML content needs to be added to be served by the Apache webserver. To do this, create a subdirectory **html** in the root directory. Inside that, a file named ``index.html`` needs to be created. Add some HTML content to ``index.html``. 

.. code-block:: console

	$ ll
	total 8
	drwxr-xr-x   4 ryan  staff  128 Jan 31 11:03 .
	drwxr-xr-x+ 31 ryan  staff  992 Jan 31 11:03 ..
	drwxr-xr-x   4 ryan  staff  128 Jan 30 18:03 .vagrant
	-rw-r--r--   1 ryan  staff  123 Jan 31 11:03 Vagrantfile

	$ mkdir html

	$ cd html

	$ touch index.html

	$ vim index.html 
	
	$ cat index.html 
	<!DOCTYPE html>
	<html>
	  <body>
	    <h1>Getting started with Vagrant!</h1>
	  </body>
	</html>


---------------
3.3.4 Provision
---------------

Once the above steps have been done, run ``vagrant up`` to create the VM and 
Vagrant should automatically provision the web server Apache with it. 

.. code-block:: console

	$ vagrant up
	...
	==> default: Mounting shared folders...
	    default: /vagrant => /Users/ryan/test
	==> default: Running provisioner: shell...
	    default: Running: /var/folders/wy/brs2xq1526v9rv_hysd5w7kh0000gn/T/vagrant-shell20200131-10236-1v9sq3l.sh
	...


.. warning:: 

	If the VM was already provisioned previously, the command ``vagrant reload 
	--provision`` needs to be run to restart the VM with the provision steps. 
	The following error might be encountered to indicate that this step is 
	needed:

	.. code-block:: console

		$ vagrant up
		...
		==> default: Machine already provisioned. Run `vagrant provision` or use the `--provision`
		==> default: flag to force provisioning. Provisioners marked to run always will still run.

		$ vagrant reload --provision
		...
		==> default: Mounting shared folders...
		    default: /vagrant => /Users/ryan/test
		==> default: Running provisioner: shell...
		    default: Running: /var/folders/wy/brs2xq1526v9rv_hysd5w7kh0000gn/T/vagrant-shell20200131-6036-1o5p645.sh
		...


Once the VM is up, ``vagrant ssh`` into the host and verify that the HTML file 
is available.

.. code-block:: console

	$ vagrant ssh
	...

	vagrant@vagrant:~$ wget -qO- 127.0.0.1
	<!DOCTYPE html>
	<html>
	  <body>
	    <h1>Getting started with Vagrant!</h1>
	  </body>
	</html>


.. note::

	The **index.html** file served under **./html/index.html** in the *local machine* is available in the *VM* -- located in both **/vagrant/html/index.html** and **/var/www/html/index/html**. 


.. tip:: 

	For complex provisioning scripts, it may be more efficient to package a custom Vagrant box with those packages pre-installed instead of building them each time. This won't be covered in this guide, but can be found in the `packaging custom boxes documentation`_.

	.. _`packaging custom boxes documentation`: https://www.vagrantup.com/docs/boxes/base.html



