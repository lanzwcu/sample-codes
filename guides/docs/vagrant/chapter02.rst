*******************
2 - Getting Started
*******************

This chapter will discuss how to get started with Vagrant by installing it.


======================
2.1 Installing Vagrant
======================

First, go to the download page of Vagrant_.

.. _Vagrant: https://www.vagrantup.com/downloads.html

Install the package using the standard procedure for your operating system. This
should add the ``vagrant`` command to your system path and should be available 
in your terminal. 

For this instance, `VirtualBox will be used and will need to be downloaded`_ and 
installed.

.. _`VirtualBox will be used and will need to be downloaded`: https://www.virtualbox.org/wiki/Downloads


===================
2.2 Testing Vagrant
===================

Once both **Vagrant** and **VirtualBox** are installed, test if a virtual machine can be
created and run. Issue the ``vagrant init`` and then ``vagrant up`` on the 
terminal:

.. code-block:: console

	$ vagrant init hashicorp/bionic64
	A `Vagrantfile` has been placed in this directory. You are now
	ready to `vagrant up` your first virtual environment! Please read
	the comments in the Vagrantfile as well as documentation on
	`vagrantup.com` for more information on using Vagrant.

	$ ll Vagrantfile 
	-rw-r--r--  1 ryan  staff  3024 Jan 29 18:28 Vagrantfile

	$ vagrant up
	Bringing machine 'default' up with 'virtualbox' provider...
	==> default: Box 'hashicorp/bionic64' could not be found. Attempting to find and install...
	    default: Box Provider: virtualbox
	    default: Box Version: >= 0
	==> default: Loading metadata for box 'hashicorp/bionic64'
	    default: URL: https://vagrantcloud.com/hashicorp/bionic64
	==> default: Adding box 'hashicorp/bionic64' (v1.0.282) for provider: virtualbox
	    default: Downloading: https://vagrantcloud.com/hashicorp/boxes/bionic64/versions/1.0.282/providers/virtualbox.box
	==> default: Box download is resuming from prior download progress
	    default: Download redirected to host: vagrantcloud-files-production.s3.amazonaws.com
	==> default: Successfully added box 'hashicorp/bionic64' (v1.0.282) for 'virtualbox'!
	==> default: Importing base box 'hashicorp/bionic64'...
	==> default: Matching MAC address for NAT networking...
	==> default: Checking if box 'hashicorp/bionic64' version '1.0.282' is up to date...
	==> default: Setting the name of the VM: Downloads_default_1580344846102_936
	Vagrant is currently configured to create VirtualBox synced folders with
	the `SharedFoldersEnableSymlinksCreate` option enabled. If the Vagrant
	guest is not trusted, you may want to disable this option. For more
	information on this option, please refer to the VirtualBox manual:

	  https://www.virtualbox.org/manual/ch04.html#sharedfolders

	This option can be disabled globally with an environment variable:

	  VAGRANT_DISABLE_VBOXSYMLINKCREATE=1

	or on a per folder basis within the Vagrantfile:

	  config.vm.synced_folder '/host/path', '/guest/path', SharedFoldersEnableSymlinksCreate: false
	==> default: Clearing any previously set network interfaces...
	==> default: Preparing network interfaces based on configuration...
	    default: Adapter 1: nat
	==> default: Forwarding ports...
	    default: 22 (guest) => 2222 (host) (adapter 1)
	==> default: Booting VM...
	==> default: Waiting for machine to boot. This may take a few minutes...
	    default: SSH address: 127.0.0.1:2222
	    default: SSH username: vagrant
	    default: SSH auth method: private key
	    default: 
	    default: Vagrant insecure key detected. Vagrant will automatically replace
	    default: this with a newly generated keypair for better security.
	    default: 
	    default: Inserting generated public key within guest...
	    default: Removing insecure key from the guest if it's present...
	    default: Key inserted! Disconnecting and reconnecting using new SSH key...
	==> default: Machine booted and ready!
	==> default: Checking for guest additions in VM...
	    default: The guest additions on this VM do not match the installed version of
	    default: VirtualBox! In most cases this is fine, but in rare cases it can
	    default: prevent things such as shared folders from working properly. If you see
	    default: shared folder errors, please make sure the guest additions within the
	    default: virtual machine match the version of VirtualBox you have installed on
	    default: your host and reload your VM.
	    default: 
	    default: Guest Additions Version: 6.0.10
	    default: VirtualBox Version: 6.1
	==> default: Mounting shared folders...
	    default: /vagrant => /Users/ryan/Downloads


After the commands finish running and downloading the image, a virtual machine
in **VirtualBox** running Ubuntu 18.04 LTS 64-bit should be available. Use 
``vagrant ssh`` to SSH into the virtual machine:

.. code-block:: console

	$ vagrant ssh
	Welcome to Ubuntu 18.04.3 LTS (GNU/Linux 4.15.0-58-generic x86_64)

	 * Documentation:  https://help.ubuntu.com
	 * Management:     https://landscape.canonical.com
	 * Support:        https://ubuntu.com/advantage

	  System information as of Thu Jan 30 00:44:22 UTC 2020

	  System load:  0.05              Processes:           91
	  Usage of /:   2.5% of 61.80GB   Users logged in:     0
	  Memory usage: 11%               IP address for eth0: 10.0.2.15
	  Swap usage:   0%

	 * Overheard at KubeCon: "microk8s.status just blew my mind".

	     https://microk8s.io/docs/commands#microk8s.status

	0 packages can be updated.
	0 updates are security updates.


	vagrant@vagrant:~$ 

	vagrant@vagrant:~$ whoami
	vagrant

	vagrant@vagrant:~$ hostname
	vagrant


Notice you are logged in as the user **vagrant** inside the virtual machine
**vagrant**.

Feel free to explore the host. Once done, log out using the ``logout`` command
while still inside the virtual machine. This will take you back to your local 
machine:

.. code-block:: console

	vagrant@vagrant:~$ logout
	Connection to 127.0.0.1 closed.

	$ 


To terminate the virtual machine, use ``vagrant destroy``:

.. code-block:: console

	$ vagrant destroy
	    default: Are you sure you want to destroy the 'default' VM? [y/N] y
	==> default: Forcing shutdown of VM...
	==> default: Destroying VM and associated drives...


=====================
2.3 Recap of Commands
=====================

To recap, use the following commands to work with **Vagrant**:

``vagrant init [options]``

	Initialize the current directory to be a Vagrant environment by creating an
	initial Vagrantfile (if one does not already exist). 


``vagrant up``

	Creates and configures virtual machines according to the specifications at
	the Vagrantfile.


``vagrant ssh [options]``

	SSH into a running Vagrant machine and gives access to its shell. When 
	multiple machines are running, use ``vagrant status`` to list the machines
	with their current state.

	.. code-block:: console

		$ vagrant status
		Current machine states:

		node1                     running (virtualbox)
		node2                     running (virtualbox)

		This environment represents multiple VMs. The VMs are all listed
		above with their current state.


	To ssh into a specific machine, for example, towards **node2**, use 
	``vagrant ssh node2``. 

	.. code-block:: console

		$ vagrant ssh node2

		Welcome to your Vagrant-built virtual machine.
		Last login: Fri Sep 14 06:23:18 2012 from 10.0.2.2

		vagrant@bionic64:~$


``vagrant destroy``

	Stops the running machine Vagrant is managing and destroys all resources 
	that were created during the machine creation process. 

	.. tip:: 

		There are other options in tearing down a machine in Vagrant. Each has 
		their own uses, advantages and disadvantages.

		``vagrant suspend``

			This will save the current running state of the machine and stop it.
			When ``vagrant up`` is ran, it will be resumed where it was last 
			left off. 

			**Pros:**

				Running ``vagrant up`` until the machine is available is very 
				quick, which usually takes about 5-10 seconds. 


			**Cons:**

				Eats up disk space. One for the VM itself, another for the state
				of the VM RAM on disk. 


		``vagrant halt``

			Gracefully shuts down the guest operating system and powers down the
			guest machine. ``vagrant up``-ing it will boot it up again. 

			**Pros:**

				Clean shut down of machine; clean boot-up. 


			**Cons:**

				Machine needs to be cold-booted which might take some time. Guest machine 
				still also consumes disk space on the host machine. 


		``vagrant destroy``

			Removes all trace of the guest machine from the host machine. It'll
			stop the guest machine, power it down, and remove all offthe guest
			hard disks.

			**Pros:**

				No cruft is left on the host machine. Disk space used by the 
				disk and RAM of	the guest machine will be reclaimed. 


			**Cons:**

				Will need more time to start even compared to issuing ``vagrant 
				up`` after ``vagrant halt`` since Vagrant has to re-import the 
				machine again and re-provision apps when configured to do so. 

