**********************
4 - Vagrant networking
**********************

This chapter will discuss how to configure the networking features so the VM can
be accessed directly via the host machine. 


===========================
4.1 Why set up the network?
===========================

The example used in the previous chapter, at this point, is already running a 
web server but can only be accessed by going inside the guest system. In order 
to access the content it is hosting - or any other service that may be setup 
from within the guest machine - from the host machine, Vagrant can be configured
to route such requests by using Vagrant's networking features. 

Using the same example, we can use the port forwarding feature which will be 
discussed below. 


===================
4.2 Port Forwarding
===================

Port forwarding can be used to specify ports on the VM or guest machine to share
to the host machine. This way, appications can be accessed locally via the host 
machine, but have all network traffic diverted to the specific port on the guest
machine. 

Continuing from the previous example, where Apache was provisioned in the VM, 
we'd want to have a way to access the HTML content being served without having 
to ``vagrant up`` then ``wget`` to get to the page. To achieve this, 
``config.vm.network`` needs to be added to the Vagrantfile. 

.. code-block:: ruby

	Vagrant.configure("2") do |config|
	  config.vm.box = "hashicorp/bionic64"
	  config.vm.provision :shell, path: "bootstrap.sh"
	  config.vm.network :forwarded_port, guest: 80, host: 4567
	end


Run ``vagrant up`` or ``vagrant reload`` - if the VM is already running - to 
have the changes take effect. 

.. code-block:: console

	==> default: Forwarding ports...
	    default: 80 (guest) => 4567 (host) (adapter 1)


Once done, access ``http://127.0.0.1:4567`` on the local browser. The web page 
should be served by the VM to the local browser. 


====================
4.3 Other Networking
====================

Vagrant can be configured to allow other forms of networking, like assigning a 
static IP address to the guest machine, or to bridge the guest machine onto an 
existing network. These will not be discussed in this guide, but can be read up 
more on Vagrant's networking_ page. 

.. _networking: https://www.vagrantup.com/docs/networking/

