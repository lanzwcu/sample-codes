.. Git Cheatsheet documentation master file, created by
   sphinx-quickstart on Mon Nov  4 10:13:27 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Git Cheatsheet
==============

.. image:: /images/git_banner.png

Git is simply a version control system for tracking changes. It allows multiple
developers to work on a project with the same base file. It also tracks who made
the change and when was it made. This document will cover some of the commonly
used git commands.


.. toctree::
   :maxdepth: 2

   chapter01
   chapter02
   chapter03
   chapter04
   chapter05
   chapter06