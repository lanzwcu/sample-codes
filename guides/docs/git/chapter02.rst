2 - Create Repositories
=======================

**git init**
  This command is used to create a new git repository or reinitialize and existing
  repository.

    syntax: ``git init <options> directory_name``

    example: ``git init git_folder``

  .. code-block:: console

    vagrant@ubuntu-bionic:~$ git init git_folder
    Initialized empty Git repository in /home/vagrant/git_folder/.git/


  In the example above, we've created a directory and initialized a git repository
  with the directory name ``git_folder``.

  If no directory name is specified, it will create a new git repository using
  the current working directory. If the directory already has git initialized,
  it will simply reinitialize it.

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git init
    Reinitialized existing Git repository in /home/vagrant/git_folder/.git/


_______________________________________________________________________________

**git clone**
  This command is used to clone an existing repository into a new directory.
  The repository can either be located on the local filesystem, remote HTTP or SSH.

    syntax: ``git clone <options> repository_name_or_HTTP_or_SSH``

    example: ``git clone https://gitlab.com/lanzwcu/clone-project.git``

  .. code-block:: console

    vagrant@ubuntu-bionic:~$ git clone https://gitlab.com/lanzwcu/clone-project.git
    Cloning into 'clone-project'...
    Username for 'https://gitlab.com': culanzw@gmail.com
    Password for 'https://culanzw@gmail.com@gitlab.com':
    remote: Enumerating objects: 3, done.
    remote: Counting objects: 100% (3/3), done.
    remote: Total 3 (delta 0), reused 0 (delta 0)
    Unpacking objects: 100% (3/3), done.


  The example above clones a project using HTTP. To clone a project using SSH,
  you first need to setup an SSH key. Below are links on how to set up one:

    - Github - https://help.github.com/en/github/authenticating-to-github/adding-a-new-ssh-key-to-your-github-account
    - Gitlab - https://docs.gitlab.com/ee/ssh/
    - Bitbucket - https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html

  To copy from a current directory we will use a couple of flags:

    - ``-l`` - tells git that the repository to clone will be coming from a local machine
    - ``-n`` - stands for "no checkout" of HEAD after cloning

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git clone -l -n . ../copy_folder
    Cloning into '../copy_folder'...
    warning: You appear to have cloned an empty repository.
    done.