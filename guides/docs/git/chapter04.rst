4 - Make Changes
================

**git status**
  This command displays the contents in the index or "staging area", we can
  view which files are not yet added to the index and see which files are ready
  to be committed.

    syntax: ``git status <options>``

    example: ``git status``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git status
    On branch master

    No commits yet

    Changes to be committed:
      (use "git rm --cached <file>..." to unstage)

            new file:   ch1.rst
            new file:   ch2.rst
            new file:   ch3.rst
            new file:   file.txt

    Untracked files:
      (use "git add <file>..." to include in what will be committed)

            new_dir/
            new_file.txt


_______________________________________________________________________________

**git log**
  This command is used to display all the commits made in the current branch you're on.

    syntax: ``git log <options>``

    example 1: ``git log``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git log
    commit 6510373480e1ad603b705a9fa5985ee4aaf330c8 (HEAD -> new_branch)
    Author: Lanz Cu <culanzw@gmail.com>
    Date:   Fri Nov 1 10:56:41 2019 +0000

        commit

    commit bab1428675f9f1dc57ccf7f40b6264c04a36992b
    Author: Lanz Cu <culanzw@gmail.com>
    Date:   Fri Nov 1 10:47:13 2019 +0000

        deleted ch2

    commit 2a8fc494a6a4f50cc58e358d76eefa1d4e715c9c (master, another_branch)
    Author: Lanz Cu <culanzw@gmail.com>
    Date:   Fri Nov 1 10:10:51 2019 +0000

        removed ch1.rst

    commit ea0cec2bd45652e5bf192e48c29f6fda60760df3
    Author: Lanz Cu <culanzw@gmail.com>
    Date:   Fri Nov 1 09:48:35 2019 +0000

        This is a commit message


  .. note:: You can limit the number of commits to be displayed by adding ``-<limit>``,
            for example: ``git log -2``.

  To have each commit displayed in one line, we use the ``--oneline`` flag:

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git log --oneline
    6510373 (HEAD -> new_branch) commit
    bab1428 deleted ch2
    2a8fc49 (master, another_branch) removed ch1.rst
    ea0cec2 This is a commit message


_______________________________________________________________________________

**git add**
  This command is used to add files to the index or the "staging area".

    syntax: ``git add <options> dir_name/filename``

    example: ``git add new_file.txt``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git add new_file.txt


  If you would like to add all files that are "unstaged", simply use the ``.`` symbol:

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git add .


  You can also add files that are of a certain type, for example you want to add
  all .rst files without having to add other file types:

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git add *.rst


_______________________________________________________________________________

**git rm**
  This command is used to remove staged files from the working tree and the index.

    syntax: ``git rm <options> dir_name/filename``

    example: ``git rm --cached new_file.txt``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git rm --cached new_file.txt
    rm 'new_file.txt'


  To remove a directory, you have to allow recursive removal using ``-r`` flag:

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git rm -r --cached new_dir
    rm 'new_dir/test.rst'


_______________________________________________________________________________

**git commit**
  This command is used to commit changes made in the index. This takes everything
  from the index and put it in the local repository.

    syntax: ``git commit <options>``

    example 1: ``git commit -m "This is a commit message"``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git commit -m "This is a commit message"
    [master (root-commit) ea0cec2] This is a commit message
     4 files changed, 0 insertions(+), 0 deletions(-)
     create mode 100644 ch1.rst
     create mode 100644 ch2.rst
     create mode 100644 ch3.rst
     create mode 100644 file.txt


  In the example above, we've committed all the staged files from our index. The
  ``-m`` flag stands for "message" and tells git that the characters in quotations
  are the commit message.

  ``git commit`` can also automatically stage files that have been changed or
  removed using the ``-a`` flag. It does not cover newly added files.
  For example, let's remove a file using ``rm`` and not ``git rm``, then add the
  ``-a`` flag.

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ rm ch1.rst
    vagrant@ubuntu-bionic:~/git_folder$ git commit -a -m "removed ch1.rst"
    [master 2a8fc49] removed ch1.rst
     1 file changed, 0 insertions(+), 0 deletions(-)
     delete mode 100644 ch1.rst


_______________________________________________________________________________

**git tag**
  This command is used to view, create, delete a tag object signed with GPG.
  GPG stands for "GNU Privacy Guard" which is a form of encryption.

    syntax: ``git tag <options>``

  To add a tag, we use the ``-a`` flag:

    example 1: ``git tag -a v1.0 -m "This is version 1.0"``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git tag -a v1.0 -m "This is version 1.0"


  To view tags, we use the following command:

    example 2: ``git tag``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git tag
    v1.0


  To delete a tag, we use the ``-d`` flag:

    example 3: ``git tag -d v1.0``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git tag -d v1.0
    Deleted tag 'v1.0' (was 0fb7485)


_______________________________________________________________________________

**git stash**
  This command is used to temporarily store changes that are not yet committed
  which can be retrieved and used later on.

    syntax: ``git stash <options> stash_name``

  To temporarily stash modified track files we use the ``save`` option:

    example 1: ``git stash save stash_name``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git stash save stash_name
    Saved working directory and index state On new_branch: stash_name


  To view the list of stash, we use the ``list`` option:

    example 2: ``git stash list``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git stash list
    stash@{0}: On new_branch: stash_name


  Now there are 2 ways to use the saved stash:

    - ``pop`` - Takes the stash and apply it on the current working directory.
      It then removes the state from the stash list.

      example 3: ``git stash pop``

    .. code-block:: console

      vagrant@ubuntu-bionic:~/git_folder$ git stash pop
      On branch new_branch
      Changes to be committed:
        (use "git reset HEAD <file>..." to unstage)

              new file:   removable.txt

      Dropped refs/stash@{0} (845aa1d0b897399fd62bb0a12ca011d37ba8be85)
      vagrant@ubuntu-bionic:~/git_folder$ git stash list


    - ``apply`` - Similar to ``pop`` but does not remove the state from the stash
      list.

      example 4: ``git stash apply``

    .. code-block:: console

      vagrant@ubuntu-bionic:~/git_folder$ git stash apply
      On branch new_branch
      Changes to be committed:
        (use "git reset HEAD <file>..." to unstage)

              new file:   removable.txt

      vagrant@ubuntu-bionic:~/git_folder$ git stash list
      stash@{0}: On new_branch: stash_name


    .. note:: When no stash name is specified, git takes the first state in the stash list.

  .. note:: When applying stash, the working directory must match the index.

