3 - Branches
============

**git branch**
  This command is used to view, create or delete branches.

    syntax: ``git branch <option> branch_name``

  To view existing branch/es, we use the following command:

    example 1: ``git branch``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git branch
    * master


  The "master" branch is the default branch created by git, the branch with the
  ``*`` tells us which branch we are on. To create a new branch we simply have to
  add a branch name:

     example 2: ``git branch new_branch``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git branch new_branch
    vagrant@ubuntu-bionic:~/git_folder$ git branch
    * master
      new_branch


  To delete an existing branch, we have to use the ``-d`` flag:

     example 3: ``git branch -d new_branch``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git branch -d new_branch
    Deleted branch new_branch (was 2a8fc49).


  .. note:: Lowercase ``-d`` flag will only work if the branch is fully merged with
            its upstream branch. To delete a branch that is not fully merged, use
            the upper case ``-D`` flag to force delete it.


_______________________________________________________________________________

**git checkout**
  This command is used to switch among branches or restore working tree files.

    syntax: ``git checkout <options> branch_name``

  To switch from one branch to another, we specify which branch we want to go to:

    example 1: ``git checkout new_branch``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git checkout new_branch
    Switched to branch 'new_branch'
    vagrant@ubuntu-bionic:~/git_folder$ git branch
    * new_branch
      master


  To restore or discard all changes made in a working tree file:

    example 2: ``git checkout -- new_file.txt``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git checkout -- new_file.txt


  .. note:: When dealing with pathspecs or files, it is highly suggested to use ``--`` as per example,
            this tells git that everything followed by those symbols are treated as
            pathspecs or files.


  ``git checkout`` can also be used to create a branch and switch to it using
  the ``-b`` flag:

    example 3: ``git checkout -b another_branch``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git checkout -b another_branch
    Switched to a new branch 'another_branch'
    vagrant@ubuntu-bionic:~/git_folder$ git branch
    * another_branch
      master
      new_branch


_______________________________________________________________________________

**git diff**
  This command is used to show the differences between commits, branches, etc.

    syntax: ``git diff <options>``

  To view the entire difference between the master branch and the current branch:

    example 1: ``git diff master``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git diff master
    diff --git a/ch2.rst b/delete.txt
    similarity index 100%
    rename from ch2.rst
    rename to delete.txt
    diff --git a/new_dir/test.rst b/new_dir/test.rst
    new file mode 100644
    index 0000000..e69de29
    diff --git a/new_file.txt b/new_file.txt
    new file mode 100644
    index 0000000..e69de29
    diff --git a/test.txt b/test.txt
    new file mode 100644
    index 0000000..e69de29
    diff --git a/~ b/~
    new file mode 100644
    index 0000000..00b4b1c
    --- /dev/null
    +++ b/~
    @@ -0,0 +1,6 @@
    +d
    +fs
    +dfs
    +dfdfsdfs
    +
    +


  To view the number of changes (in lines) of each files we use the ``--stat`` flag:

    - ``+`` signs are additions
    - ``-`` signs are deletions
    - the number before the signs are the number of lines that are different.

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git diff --stat master
     ch4.rst   |  0
     extra.txt | 15 +++++++++++++++
     2 files changed, 15 insertions(+)


  To view changes between the staging area and the ``HEAD``:

    example 3: ``git diff --staged``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git diff --staged
    diff --git a/removable.txt b/removable.txt
    new file mode 100644
    index 0000000..e69de29


