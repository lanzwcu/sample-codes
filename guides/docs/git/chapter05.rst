5 - Reset Commits
=================

**git reset**
  This command is used to revert changes made to a specified state.

    syntax: ``git reset <options> file_name/commit``

  To unstage a file and preserve its contents, we use the following command:

    example 1: ``git reset removable.txt``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git status
    On branch new_branch
    Changes to be committed:
      (use "git reset HEAD <file>..." to unstage)

            new file:   removable.txt

    Untracked files:
      (use "git add <file>..." to include in what will be committed)

            ch4.rst
            ch5.rst

    vagrant@ubuntu-bionic:~/git_folder$ git reset removable.txt
    vagrant@ubuntu-bionic:~/git_folder$ git status
    On branch new_branch
    Untracked files:
      (use "git add <file>..." to include in what will be committed)

            ch4.rst
            ch5.rst
            removable.txt

    nothing added to commit but untracked files present (use "git add" to track)


  Here we can see ``removable.txt`` is staged in our first ``git status``, then it
  became an untracked file after ``git reset``


_______________________________________________________________________________

  To reset back to a commit and preserve the changes locally, we simply have to
  specify the commit:

    example 2: ``git reset 02a729a``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git add ch4.rst
    vagrant@ubuntu-bionic:~/git_folder$ git commit -m "add ch4.rst"
    [new_branch 96f9bb2] add ch4.rst
     1 file changed, 0 insertions(+), 0 deletions(-)
     create mode 100644 ch4.rst
    vagrant@ubuntu-bionic:~/git_folder$ git status
    On branch new_branch
    Untracked files:
      (use "git add <file>..." to include in what will be committed)

            ch5.rst

    nothing added to commit but untracked files present (use "git add" to track)
    vagrant@ubuntu-bionic:~/git_folder$ git reset 02a729a
    vagrant@ubuntu-bionic:~/git_folder$ git status
    On branch new_branch
    Untracked files:
      (use "git add <file>..." to include in what will be committed)

            ch4.rst
            ch5.rst

    nothing added to commit but untracked files present (use "git add" to track)


  In this example, we made a commit that removed ``ch4.rst`` and reset it back
  to the commit before ``ch4.rst`` was added.


_______________________________________________________________________________

  To reset back to a specific commit and discard all history, we just have to
  add the ``--hard`` option.

    example 3: ``git reset --hard 02a729a``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git add ch5.rst
    vagrant@ubuntu-bionic:~/git_folder$ git commit -m "add ch5.rst"
    [new_branch f16f219] add ch5.rst
     1 file changed, 0 insertions(+), 0 deletions(-)
     create mode 100644 ch5.rst
    vagrant@ubuntu-bionic:~/git_folder$ git status
    On branch new_branch
    Untracked files:
      (use "git add <file>..." to include in what will be committed)

            ch4.rst

    nothing added to commit but untracked files present (use "git add" to track)
    vagrant@ubuntu-bionic:~/git_folder$ git reset --hard 02a729a
    HEAD is now at 02a729a remove removable.txt
    vagrant@ubuntu-bionic:~/git_folder$ ls
     ch3.rst   ch4.rst   delete.txt   new_dir   new_file.txt
    vagrant@ubuntu-bionic:~/git_folder$ git status
    On branch new_branch
    Untracked files:
      (use "git add <file>..." to include in what will be committed)

            ch4.rst

    nothing added to commit but untracked files present (use "git add" to track)


  In this example, we added and committed ``ch5.rst``. After using ``git reset --hard``
  , ``ch5.rst`` can no longer be found in  both the unstaged files and the local files.

  .. warning:: Be extra cautious when changing the history of a repository, otherwise
               you might end up losing important codes/files.