1 - Configuration
=================

**git config**
  This command is used to acquire and set a repository's options. It is commonly
  used to set the author's name and email address that will be used with your commits.
  There are 3 configuration levels in git:

    - ``--local`` - By default git will write to a local level if no configuration level is specified.
    - ``--global`` - Global level configuration level is applied to an OS user.
    - ``--system`` - System level is applied across the entire machine, it covers all users on an OS and all repos.

    syntax 1: ``git config <options> user.name "name_value"``

    example 1: ``git config --global user.name "Lanz Cu"``

    syntax 2: ``git config <options> user.email "email_value"``

    example 2: ``git config --global user.email "lanzwcu@yahoo.com"``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git_folder$ git config --global user.name "Lanz Cu"
    vagrant@ubuntu-bionic:~/git_folder$ git config --global user.email "culanzw@gmail.com"


  To open and manually edit the global configuration file:

    example 3: ``git config --global edit``

  A screen will be displayed where you can manually edit the contents.

  .. code-block:: console

      GNU nano 2.9.3                                   /home/vagrant/.gitconfig
    [user]
            email = lanzcu@novostormtech.com
            name = Lanz Cu




    ^G Get Help    ^O Write Out   ^W Where Is    ^K Cut Text    ^J Justify     ^C Cur Pos     M-U Undo       M-A Mark Text
    ^X Exit        ^R Read File   ^\ Replace     ^U Uncut Text  ^T To Spell    ^_ Go To Line  M-E Redo       M-6 Copy Text





