6 - Synchronize Changes
=======================

**git merge**
  This command is used to combine two or more branches together. After merging,
  all commits and its authors will be part of the git log.

    syntax: ``git merge <options> branch_name``

    example: ``git merge feature -m "Merge branches"``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git$ git merge feature -m "Merge branches"
    Merge made by the 'recursive' strategy.
     new_file.txt | 6 ++++++
     1 file changed, 6 insertions(+)
     create mode 100644 new_file.txt


_______________________________________________________________________________

**git rebase**
  This command is used reapply commits on top of another base. After rebasing,
  only 1 commit from a developer will be part of the git log. This is used by
  developers to have a cleaner history.

    syntax: ``git rebase <option> branch_name``

    example: ``git rebase master``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/git$ git rebase master
    First, rewinding head to replay your work on top of it...
    Applying: add another_file.txt:


_______________________________________________________________________________

**git fetch**
  This command is used to fetch branches, tags, histories, etc from another repository.
  It tells git to gather all commits that are not in your current branch and store
  them locally. It does not merge with your current branch.

    syntax: ``git fetch <options> repository branch_name``

    example: ``git fetch https://gitlab.com/lanzwcu/clone-project.git``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/clone-project$ git fetch https://gitlab.com/lanzwcu/clone-project.git
    Username for 'https://gitlab.com': culanzw@gmail.com
    Password for 'https://culanzw@gmail.com@gitlab.com':
    From https://gitlab.com/lanzwcu/clone-project
     * branch            HEAD       -> FETCH_HEAD


  .. note:: To fetch a specific branch only, simply add the branch name after
            the repository, like this: ``git fetch https://gitlab.com/lanzwcu/clone-project.git new_branch``


_______________________________________________________________________________

**git pull**
  This command is simply a ``git fetch`` directly followed by a ``git merge``.
  It is frequently used to update the local copy with new commits.

    syntax: ``git pull <options> repository``

    example: ``git pull https://gitlab.com/lanzwcu/clone-project``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/clone-project$ git pull https://gitlab.com/lanzwcu/clone-project
    Username for 'https://gitlab.com': culanzw@gmail.com
    Password for 'https://culanzw@gmail.com@gitlab.com':
    warning: redirecting to https://gitlab.com/lanzwcu/clone-project.git/
    remote: Enumerating objects: 4, done.
    remote: Counting objects: 100% (4/4), done.
    remote: Compressing objects: 100% (2/2), done.
    remote: Total 3 (delta 0), reused 0 (delta 0)
    Unpacking objects: 100% (3/3), done.
    From https://gitlab.com/lanzwcu/clone-project
     * branch            HEAD       -> FETCH_HEAD
    Updating e1e0d3a..1b66ab5
    Fast-forward
     new_file | 1 +
     1 file changed, 1 insertion(+)
     create mode 100644 new_file


  The main difference between ``fetch`` and ``pull`` is that ``pull`` automatically
  merges the commits without having to review them.

_______________________________________________________________________________

**git push**
  This command is used to update the remote repository with the committed objects
  from you local files.

    syntax: ``git push <options> repository``

    example: ``git push origin fetch``

  .. code-block:: console

    vagrant@ubuntu-bionic:~/clone-project$ git push origin fetch
    Username for 'https://gitlab.com': culanzw@gmail.com
    Password for 'https://culanzw@gmail.com@gitlab.com':
    Total 0 (delta 0), reused 0 (delta 0)
    remote:
    remote: To create a merge request for fetch, visit:
    remote:   https://gitlab.com/lanzwcu/clone-project/merge_requests/new?merge_request%5Bsource_branch%5D=fetch
    remote:
    To https://gitlab.com/lanzwcu/clone-project.git
     * [new branch]      fetch -> fetch


  In the example above, we pushed a new branch called ``fetch`` into our remote
  repository.
