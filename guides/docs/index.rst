.. Vagrant Guide documentation master file, created by
   sphinx-quickstart on Wed Jan 29 14:38:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Novostorm Documents
===================

.. image:: /images/novostorm_banner.png

.. toctree::
   :maxdepth: 1

   /git/index.rst
   /linux/index.rst
   /postgresql/index.rst
   /vagrant/index.rst



