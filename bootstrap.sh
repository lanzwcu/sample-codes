apt-get update

apt-get upgrade -y

apt-get install -y git

apt-get install -y python3 python3-pip

apt-get install -y postgresql

# cd laparagon

# sudo pip3 install -r requirements.txt

# Materialize installation
# sudo pt-key adv --keyserver keyserver.ubuntu.com --recv-keys 379CE192D401AB61
# sudo sh -c 'echo "deb http://packages.materialize.io/apt/ /" > /etc/apt/sources.list.d/materialize.list'
# sudo apt update
# sudo apt install materialized


# DB initialize
# sudo -u postgres psql -c "CREATE DATABASE paragon_db;"
# sudo -u postgres psql -c "CREATE USER lanz WITH PASSWORD 'password';"
# sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE paragon_db TO lanz;"

# cd project

# Database migration
# python3 manage.py migrate
